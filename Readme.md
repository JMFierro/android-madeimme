## App Android - MadeInMe ##
### Interfaz para el diseño de un zapato ###

Aplicación que implementa todo lo necesario para una interfaz con la que diseñar una zapato. Trabaja con un 'Modelo Dummy', en la que los datos no son reales, AUNQUE HAY  INTERACCION CON UNA API DE PRUEBA HECHA POR MI (1), La clase 'ModelDummy' que proporciona los datos que necesita y simula los métodos que deberá implementar el MODELO REAL que trabaje con la API, grabe en disco y haga toda la gestión de datos.

 

## Trabajo realizado ###

1.- PASAR EL DISEÑO DE LA 'WEB MadeInMe' AL DE LA *'APP'*: 

- La disposición de lo que yo llamo *'Paleta'* es diferente a la de la web. He querido hacerla más práctica y adaptada al toque. 

- Otro aspecto que he adaptado son las pestallas, en *'Android*' he utilizado las de *'texto y línea'* que se usan en 'pageview' y en *'iOS'* un 'SegemengControl'. Lo he complementado con un 'MenuPath' para añadir los 'Adornos y Extras'. 

- También he añadido funcionalidades al zapato de *toque* para seleccionar distintas partes, y de *'pannig'* para mover el ángulo del zapato.

2.- PROGRAMACIÓN App: Toda el interfaz. La totalidad del código es propio, realizado por mí a excepción del *'Gridwiew con secciones'* y le *'Menu Path'* en el caso de Android, y el 'Menu Path' en  'iOS'. Es código fuente 'Open Source' y especifico el origen.

3.- PROGRAMACIÓN API: API de pureba hecha por mi en **Django** y desplegada en **Heroku**.

En Heroku, al menos en la configuración gratuita, no hay  persistencia de las imagenes. Para subsanarlo se me ha ocurrido una combianción de **Tinymce**

	from tinymce import models as tinymce_models
	...
	assets = tinymce_models.HTMLField() 

y el *hosting* para el almacenamiento de imágenes **http://www.subirimagenes.net/**

4.- MODELO DUMMY: la interfaz para alimentarse de los datos tira de métodos que implementa la clase *'ModelDummy*'. Son públicos y deberán ser los que implemente el  'MODELO REAL' definitivo. De esta manera el resto del código no se vera afectado con la incorporación del 'MODELO REAL' que trabaje con la API, el disco y lo que crea oportuno para la completa gestión de los datos. 

5.- DOCUMENTACIÓN: Readme.md y documentación del código. 


## API (1) 
 Interacción con una API que he hecho en **Django** y desplegado en **Heroku**. 

En Heroku, al menos en la configuración gratuita, *no hay  persistencia de las imagenes*. Se recomienda utilizar **Amazon s3**, pero hay que proporcinar la tarjeta de crédito. **Como alternativa se me ha ocurrido** una combianción de **Tinymce**

	from tinymce import models as tinymce_models
	...
	assets = tinymce_models.HTMLField() 

y el *hosting* para el almacenamiento de imágenes **http://www.subirimagenes.net/**. 

**Tinymce** proporciona un campo con características *HTML* donde copio las imagenes almacenadas en **subirimagenes**. Con ello tengo los *links de acceso a las imágenes* para su descarga, pero a la vez se pueden **visualizar desde el mismo panel de administración de Django**, y no ver sólo un link.

En Android recupero el campo con formato HTML y extraigo los link:

				// Load images
				String bodyHtmlString = jObjectFields.getString("assets");
				Matcher m = Pattern.compile(" (?:href|src)=\"([^\"]+)").matcher(bodyHtmlString);
				while (m.find()) {
					String urlStringImage = m.group(1); 
					 
					URL url = new URL(urlStringImage);
					InputStream inputStream = url.openConnection().getInputStream();
					Bitmap bmp = BitmapFactory.decodeStream(inputStream);
					  
					mBitmapList.add(bmp);

					System.out.println(m.group(1));
				}

### Uso de la api ###

- **Tipos :** showTypesOfShoepartGridview(familyIndex,...) -> apiConnect(url...) -> ...

- **Subpars :** showShoeSubpartsHorizontalScroll(familyIndex,...) -> apiConnect(url...) -> ...


*apiConnect(url...) de **PaletteFragment.java** ejecuta en un hilo la conexión a la api:*

1. -> model.apiConnect(url): 
1. -> model.getSection(0) :*obtiene un ArrayList de bitmaps.*
1. -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*


*a continuación la secuencia de llamadas sigue asi:*

... -> model.apiConnect(url) -> new ApiJMFierroMadeInMe(url...) 

*Mientras tanto en **PalleteFragment.java** esta a la espera de que la API termine las descarga de datos, entonces se manda una mensaje a un **Handler mHandler = new Handler()** con un tag que identifica los datos descargados:* 


	/* -------------------------------------------------------------
	 * Recibe el aviso cuando un hilo de descarga(api) ha terminado.
	  --------------------------------------------------------------*/
	private Handler mHandler = new Handler(){
    	
    	@Override
        public void handleMessage(Message msg) {
        
            View gridview;
            ArrayList<Bitmap> dataArrayList;
            View progress;
    		switch(msg.what){
    		
    		/* 
    		 * Aviso de que la api de descarga para 'Types' esta completado
    		 */
            case ModelDummy.DOWNLOAD_TYPES_COMPLETED:

        		dataArrayList = (ArrayList<Bitmap>) msg.obj;
        		showTypesOfShoepartGridview (dataArrayList, mShoeFamilyCurrent, mShoepartCurrent);
				...

        		break;


       		/* 
       		 * Aviso de que la api de descarga para 'Subparts' esta completado
       		 */
            case ModelDummy.DOWNLOAD_SUBPARTS_COMPLETED:

            	dataArrayList = (ArrayList<Bitmap>) msg.obj;
				showShoeSubpartsHorizontalScroll(dataArrayList, mViewPalette, mShoeFamilyCurrent, mShoepartCurrent, 
						getTypePositionCurrent(), getSubpartPositionCurrent());
				...

##### Métodos: #####

    public void showTypesOfShoepartGridview (final int aShoeFamilyIndex, final int aShoePartIndex) {

	private void showShoeSubpartsHorizontalScroll (	
			final View aViewPalette, 
			final int aShoeFamilyIndex, 
			final int aShoePartIndex, 
			final int aTypeOfShoepartIndex, 
			final int aSubpartIndex) {

... apiConnect() de **PaletteFragment.java**
	  
		 * Conexión a la api para descarga de imagenes. 
		 * Al finalizar la descarga avisa enviando un mensaje a 'mHandler'.
		 * 
		 * @param urlString				-> url de la api.
		 * @param idView				-> view donde se visualizan los datos.
		 * @param idProgressBar			-> progress de espera"
		 * @param tagHandlerCompleted	-> tag para identificarse en 'handle' cunado haya completado la descarga.	
		 * 
	    public void apiConnect (final String urlString, int idView, int idProgressBar, final int tagHandlerCompleted)

... llamadas al modelo *(desde apiConnect()  de **PaletteFragment.java**)*:

		final ModelDummy model = new ModelDummy(mContext);
		new Thread(new Runnable() {

			@Override
			public void run() {

				model.apiConnect(urlString);
				ArrayList<Bitmap> dataArrayList = model.getSection(0);

				// Aviso de que la descarga ha fianlizado
				Message msg = Message.obtain();
				msg.what = tagHandlerCompleted;
				msg.obj = dataArrayList;
				mHandler.sendMessage(msg);

			}
		}).start();


... apiConnect() en **ModelDummy.java**:

	public void apiConnect(String urlString) {

			...

			/* --------------------
			 * Descarga de imagenes
			   --------------------*/
			// Api conexión y descarga.
			//	        	ApiJMFierroMadeInMe api = new ApiJMFierroMadeInMe("http://hidden-reef-8347.herokuapp.com/api/types_toecap/", mContext);
			ApiJMFierroMadeInMe api = new ApiJMFierroMadeInMe(urlString, mContext);
			
			// Acceso de imagenes descargadas (ArrayList de bitmaps)
			ArrayList<Bitmap> bitmapArrayList = api.getBitmap();
			
			// Guardando imagenes
			if (mDataHashMap != null && bitmapArrayList != null)
				mDataHashMap.put("", bitmapArrayList);	
	}

ApiJMFierroMadeInMe.java:

	public ApiJMFierroMadeInMe(String urlString, Context context) throws IOException {

		this.mUrlString = urlString;
		this.mContext = context;
		this.mBitmapList = new ArrayList();

		String json = getJSONFromUrlString(urlString);
		madeInMeParseJSON(json);

	}




## Especificaciones Android ##

- Compatible desde la API 8 hasta API 21.  
- Trabaja con la librería de compatibilidad support-v7.


## Instrucciones app ##

  Interfaz para el diseño de un zapato. Se utiliza un 
  **'Modelo Dummy'** que porporciona *datos ficticios* sin conexión con la API.
  La clase 'ModelDummy' provee de datos necesarios a la app y tiene los 
  métodos públicos necesarios que son los que deberán tener el 'MODELO REAL' 
  que trabaje con la API y gestione los datos. 
  
  Pasos para diseñar zapato:

   	1.- Ir seleccionando las 'Partes del zapato' (con las pestañas ó sobre zapato).
   	2.- Elegir un 'Tipo' de los que ofrece cada parte.
   	3.- A su vez cada tipo despliega una o más 'Subpartes', que son las que hay que diseñar.
   	4.- Finalmente se aplica el 'Material' de nuestra preferencia.

Se puede **girar el zapato** y elegir una **parte** tocando sobre él.

El **tipo** que se elige se muestra tambien en la parte de arriba. 

Los materiales seleccionados por el usuario de van guardando en **'Mis Materiales'**.

El **'Estado de la configuración'** o progreso indica el avance en porcentaje del diseño , barra de progreso y  circulo de progreso.


## Notas sobre el Diseño ##

Hay elementos que se han dispuesto sólo a modo de sugerencia para tomar una decisión posteriormente sobre ellos.

Se ha *triplicado* las vista de **progreso**: En letra, en forma de barra y circular. Se podrán eliminar dos de ellos, o mantener el texto y una de las barras. Incluso se podrían utilizar los tres mostrando información distinta, informando al usuario de algo adicional.
 
El **'RadioGroup circle'** muestra la parte del zapato activada. Es redundante porque ya se muestra en las pestañas (Cuerpo, Puntera, ...). Se podría utilizar para dejar 'marcada' la pestaña en la que ya se haya entrado, dejando de funcionar como un 'RadioButton' y hacerlo como un marcador.  

Esto último también se podría conseguir marcando las pestañas o cambiandolas de color.

Los background en Android e iOS (son diferentes) los he *ideado* y *creado* yo mismo.


# Modelo #

Los datos (imágenes) son fijas, estan en el directorio 'assets' y el de 'drawer' en recursos. 

En la mayoría de los casos las he sacado **directamente de la web de 'MadeInMe'** (Tipos, Subpartes, Materiales; distintos ángulos del zapato, logo). 

En otros las he **manipulado** *(logo de '*Ver detalles*)* o **creado** *(background, adornos y extras de pathMenu, trazo del selector sobre el zapato)*  con Gimp, illustrator en windows y sheashore en Mac (NO PUDE UTILIZAR LOS EDITORES QUE USAMOS EN CLASE DE DISEÑO PORQUE EL PERIODO DE PRUEBA HABÍA PASADO ?!!)


### Descripción del modelo ###

 El Modelo contiene los datos de un gridView o Gallery, 
para ello utiliza un HashMap.

    	-> private LinkedHashMap<String, ArrayList<Bitmap>> mDataHashMap = new LinkedHashMap<String, ArrayList<Bitmap>>();

*Por ejemplo para el gridview de los 'tipos' en PaletteFragment.java se crea un objeto:*


- **Tipos :** showTypesOfShoepartGridview(familyIndex,...) -> apiConnect(url...) -> ...

- **Subpars :** showShoeSubpartsHorizontalScroll(familyIndex,...) -> apiConnect(url...) -> ...


*apiConnect(url...) de **PaletteFragment.java** ejecuta en un hilo la conexión a la api:*

1. -> model.apiConnect(url): 
1. -> model.getSection(0) :*obtiene un ArrayList de bitmaps.*
1. -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*


*a continuación la secuencia de llamadas sigue asi:*

... -> model.apiConnect(url) -> new ApiJMFierroMadeInMe(url...) 


*Mientras tanto en **PalleteFragment.java** esta a la espera de que la API termine las descarga de datos, entonces se manda una mensaje a un **Handler mHandler = new Handler()** con un tag que identifica los datos descargados:* 


	/* -------------------------------------------------------------
	 * Recibe el aviso cuando un hilo de descarga(api) ha terminado.
	  --------------------------------------------------------------*/
	private Handler mHandler = new Handler(){
    	
    	@Override
        public void handleMessage(Message msg) {
        
            View gridview;
            ArrayList<Bitmap> dataArrayList;
            View progress;
    		switch(msg.what){
    		
    		/* 
    		 * Aviso de que la api de descarga para 'Types' esta completado
    		 */
            case ModelDummy.DOWNLOAD_TYPES_COMPLETED:

        		dataArrayList = (ArrayList<Bitmap>) msg.obj;
        		showTypesOfShoepartGridview (dataArrayList, mShoeFamilyCurrent, mShoepartCurrent);
				...

        		break;

### Métodos públicos del modelo necesarios para la interfaz ###

  	** ......................................
	 * Conexión a la api.
	  ---------------------------------------*
	public Bitmap apiConnect(String urlString) 


	*-------------------------------
	 * Devuelve la imagen renderizada del zapato.
	  -------------------------------*
	public Bitmap getShoeRender (int familyIndex, int shoeparIndex, int typeIndex,


	*-------------------------------
	 * Carga de Tipos de una parte del zapato.
	  -------------------------------*
	public ArrayList<Bitmap> loadTypesOfShoepart(Context context, int shoeFamilyIndex, int shoePartIndex)


	*-------------------------------
	 * Carga de Subpartes de un Tipo.
	  -------------------------------*
	public ArrayList<Bitmap> loadSubpartsOfType(Context context, int shoeFamilyIndex, int shoeparIndex, int typeOfShoepartIndex) 


	*----------------------------------------
	 * Carga de las Secciones de Materiales.
	  ---------------------------------------*
	public LinkedHashMap<String, ArrayList<Bitmap>> loadSectionsMaterials(Context context, int shoeFamilyIndex, int shoepartIndex, int typeOfShoepartIndex, int shoeSubpartIndex) 
	

	**-------------------------
	 * Carga de mis materiales.
	  -------------------------*
	public ArrayList<Bitmap> loadMyMaterials(int shoeFamilyIndex) {

	
	**.............................................
	 * Añade item desde otro modelo
	 * (ej.- modelMaterials -> modelMyMaterials)
	 * 
	 * Lo añade a la sección 0
	  .............................................*
	public void addItemFromModel(ModelDummy modelSource, int sectionIndex, int positionIndex) 
		
	
	**--------
	 * Varios.
	  --------*
	public void addItem(Bitmap itemBitmap, int sectionIndex, int positionIndex) {
	public Bitmap getItem(int sectionIndex, int positionIndex) {
	public ArrayList<Bitmap> getSection (int sectionIndex) {
	public static String getShoepartNameFolder (int position) 


