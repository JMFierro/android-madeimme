package com.utad.madeinme.api;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html.ImageGetter;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.utad.madeinme.utils.HttpUtils;
/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */
public class ApiJMFierroMadeInMe {

	private String mUrlString; 
	private Context mContext;
	private ArrayList mBitmapList;

	public ApiJMFierroMadeInMe(String urlString, Context context) throws IOException {

		this.mUrlString = urlString;
		this.mContext = context;
		this.mBitmapList = new ArrayList();

		String json = getJSONFromUrlString(urlString);
		madeInMeParseJSON(json);

	}

	public ArrayList<Bitmap> getBitmap() {
		return mBitmapList;
	}

	private String getJSONFromUrlString(String urlString) throws IOException {

		HttpUtils httpUtils = new HttpUtils(urlString);
		httpUtils.connect();
		String json = httpUtils.getString();
		return json;

	}
	
	private void madeInMeParseJSON (String jsonString) {
		// Parse JSON
		try {

			Log.e("", "--------- JSON ---------");
			JSONArray jArray = new JSONArray(jsonString);
			for (int i = 0;i < jArray.length();i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				JSONObject jObjectFields = jObject.getJSONObject("fields");

				String title = jObjectFields.getString("title");
				// String imagePath = jObjectFields.getString("image");

				// Load images
				String bodyHtmlString = jObjectFields.getString("assets");
				Matcher m = Pattern.compile(" (?:href|src)=\"([^\"]+)").matcher(bodyHtmlString);
				while (m.find()) {
					String urlStringImage = m.group(1); 
					
					URL url = new URL(urlStringImage);
					InputStream inputStream = url.openConnection().getInputStream();
					Bitmap bmp = BitmapFactory.decodeStream(inputStream);
					  
					mBitmapList.add(bmp);

					System.out.println(m.group(1));
				}

				Log.e("title", title);

				// Load imagen
				/**
				String imageUrl = "http://sheltered-eyrie-8416.herokuapp.com/media/"+imagePath;
				Log.e("imageUrl", imageUrl);
				URL url = new URL(imageUrl);
				Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
				//				int j=0;
				//				imageView.setImageBitmap(bmp);
				 * 
				 */
			}

		}
		catch (Exception e) {
			Log.e("log_tag", "Error pasrse JSON");
		}

	}

	
}




