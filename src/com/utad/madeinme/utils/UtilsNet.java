package com.utad.madeinme.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat; 
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;

/**
 * @author Jose Manuel Fierro Conchouso, 2014
 * 
 * @serial
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */
public class UtilsNet {



	/**=======================================
	 * Comprueba de hay conexi�n de internet.	
	 * ====================================== */
	static public boolean isNet(Context context) {
		// Gets the URL from the UI's text field.
		//String stringUrl = urlText.getText().toString();
		//        ConnectivityManager connMgr = 
		//        		(ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		ConnectivityManager connMgr = 
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		//        ConnectivityManager connMgr = 
		//        		(ConnectivityManager) Config.mContextMain.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected() 
				&& networkInfo.isAvailable() 
				&& networkInfo.isConnectedOrConnecting()) {
			boolean isAvailable = networkInfo.isAvailable();
			boolean isConnectedOrConnecting = networkInfo.isConnectedOrConnecting();
			boolean faivoler = networkInfo.isFailover();
			boolean romaing = networkInfo.isRoaming();
			State state = networkInfo.getState();

			//new DownloadWebpageTask().execute(stringUrl);
			return true;
		} else {
			return false;
		}

	}


	/**====================
	 *  De Stream a String
	 *=====================*/
	static public String stringJSONfromStream(InputStream streamJSON) {

		BufferedReader buffReader = null;
		buffReader = new BufferedReader(new InputStreamReader(streamJSON,Charset.forName("UTF-8")));

		StringBuffer buffer = new StringBuffer();
		String s = null;
		try {
			while((s = buffReader.readLine()) != null) {
				buffer.append(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String stringJSON = buffer.toString();
		return stringJSON;
	}

	/**===============================
	 * Fecha actual del sistema
	 * @return
	 *================================*/
	static public String getFechaActual() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy\nHH:mm:ss");
		//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return sdf.format(new Date());
	}



	static public String quitarDecimales (String numeroInString) {

		if (numeroInString == null)
			return "";
		
		String numeroOutString = null;
		int lastDot = numeroInString.lastIndexOf('.');
		if (lastDot == -1) {
			// No dots - what do you want to do?
		} else {
			numeroOutString = numeroInString.substring(0,lastDot);
		}

		if (numeroOutString != null)
			return numeroOutString;
		else
			return numeroInString;

	}
	
	
	//reinicia una Activity
	public static void reiniciarActivity(Activity actividad){
	        Intent intent=new Intent();
	        intent.setClass(actividad, actividad.getClass());
	        //llamamos a la actividad
	        actividad.startActivity(intent);
	        //finalizamos la actividad actual
	        actividad.finish();
	}
	
	
	public static void upHome (Activity thisActivity) {

		Intent intentBack = NavUtils.getParentActivityIntent(thisActivity);

		if (NavUtils.shouldUpRecreateTask(thisActivity, intentBack)) {
			// This activity is NOT part of this app's task, so create a new task
			// when navigating up, with a synthesized back stack.
			TaskStackBuilder.create(thisActivity)
			// Add all of this activity's parents to the back stack
			.addNextIntent(intentBack)
			// Navigate up to the closest parent
			.startActivities();
		}
		else {
			NavUtils.navigateUpTo(thisActivity, intentBack); 
		}

	}
	
	
	public static InputStream HttpRequest(String strUrl) {

	    HttpResponse responce = null;
	    try {
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        HttpGet request = new HttpGet();
	        request.setURI(new URI(strUrl));
	        responce = httpClient.execute(request);
	        HttpEntity entity = responce.getEntity();
	        return entity.getContent();
	    } catch (ClientProtocolException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    } catch (NullPointerException e) {
	        e.printStackTrace();
	    }
	    return null;
	}
}