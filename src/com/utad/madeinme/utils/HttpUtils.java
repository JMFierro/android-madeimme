package com.utad.madeinme.utils;

/*
 * Jon Ander Rodriguez Hidalgoo, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public class HttpUtils {

	public static final int IO_BUFFER_SIZE = 1024;
	public static final int HTTP_CONNECT_TIMEOUT_MS = 15000; // millisecons
	public static final int HTTP_READ_TIMEOUT_MS = 10000; // millisecons
	
	private String mUrl;
	private Charset mCharset;
	private HttpURLConnection mConnection;
	private InputStream mInputStream;
	
	private byte[] mBytes = null;
	
	public HttpUtils(String mUrl) {
		this(mUrl, Charset.forName("UTF-8") );
	}	
	
	public HttpUtils(String mUrl, Charset mCharset) {
		super();
		this.mUrl = mUrl;
		this.mCharset = mCharset;
	}
	
	public void connect() throws IOException {
		URL url = new URL(mUrl);
		mConnection = (HttpURLConnection) url.openConnection();
		mConnection.setConnectTimeout(HTTP_CONNECT_TIMEOUT_MS);
		mConnection.setReadTimeout(HTTP_READ_TIMEOUT_MS);
		mConnection.setRequestMethod("GET");
		mConnection.setDoInput(true);
		
		// Start connection
		mConnection.connect();
		mInputStream = mConnection.getInputStream();
	}
	
	public String getString() throws IOException {
		InputStreamReader reader = new InputStreamReader(mInputStream, mCharset);
		
		StringBuffer retorno = new StringBuffer();
		char[] buffer = new char[1024];
		int leido;
		while ((leido = reader.read(buffer)) != -1) {
			retorno.append(buffer, 0, leido);
		}

		return retorno.toString();
	}
	
	public JSONObject getJSONObject() throws JSONException, IOException {
		return new JSONObject( getString() );
	}
	
    public Bitmap getImage() throws IOException, InterruptedException {
//    	return BitmapFactory.decodeStream(mInputStream);
    	byte[] bytes = getBytes();
    	if (bytes == null) {
    		return null;
    	}
    	return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
    
    public Bitmap getBitmap() {
		return BitmapFactory.decodeStream(mInputStream);

    }
    
    public byte[] getBytes() throws IOException, InterruptedException {
    	
    	if (this.mBytes != null) {
    		return this.mBytes;
    	}
    	
    	byte[] buffer = new byte[IO_BUFFER_SIZE];
    	ByteArrayOutputStream output = new ByteArrayOutputStream();
    	
    	int leido;
    	while ((leido = mInputStream.read(buffer)) != -1) {
    		if (Thread.interrupted()) {
    			throw new InterruptedException();
    		}
    		output.write(buffer, 0, leido);
    	}
    	try {
    		this.mBytes = output.toByteArray();
    		return this.mBytes;
    	}
    	finally {
    		if (output != null) {
    			output.close();
    		}
    	}
    }
	
	public void close() {
		if (mInputStream != null) {
			try {
				mInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (mConnection != null) {
			mConnection.disconnect();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.close();
		super.finalize();
	}

	
	public InputStream getInputStream () {
		
		return mInputStream;
	}
}



























