package com.utad.madeinme;


import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.utad.madeinme.fragments.BuyDetailsFragment;
import com.utad.madeinme.fragments.PaletteFragment;
import com.utad.madeinme.fragments.PaletteSegmentTabsFragment;
import com.utad.madeinme.fragments.ShoeFragment;
import com.utad.madeinme.fragments.TabsBtnFragment;
import com.utad.madeinme.jmf.gallery.horizontal.JMFHorizontalScrollGallery;
import com.utad.madeinme.jmf.model.ModelDummy;
import com.utad.madeinme.menupath.SatelliteMenu;
import com.utad.madeinme.menupath.SatelliteMenu.SateliteClickedListener;
import com.utad.madeinme.menupath.SatelliteMenuItem;
import com.utad.madeinme.utils.Utils;
import com.utad.madeinme.utils.UtilsNet;


/**
 *<br> @author Jose Manuel Fierro Conchouso, 2014
 *<br> 
 *<br> Pr�ctica MadeInMe
 *<br> Master Desarrollo de apps para smartphone y tablet
 *<br> U-Tad
 *<br> 
 *<br> 
 *<br> Interfaz necesaria para el dise�o de un zapato. Se utiliza un 
 *<br> 'Modelo Dummy' que porporciona datos ficticios sin conexi�n con la API.
 *<br> La clase 'ModelDummy' provee de datos necesarios a la app y tiene los 
 *<br> m�todos p�blicos necesarios que son los que deber�n tener el 'MODELO REAL' 
 *<br> que trabaje con la API y gestione los datos. 
 *<br> 
 *<br> App para el dise�o de una zapato.

 *<br>  	1.- Ir seleccionando las 'Partes' del zapato (con las pesta�as � sobre zapato).
 *<br>  	2.- Elegir un 'Tipo' de los que ofrece cada parte.
 *<br>  	3.- A su vez cada tipo despliega una o m�s 'Subpartes', que son las que hay que dise�ar.
 *<br>  	4.- Finalmente se aplica el 'Material' de nuestra preferencia.

 *<br>  
 *<br>  
 *<br> ______________________________________________________________ 
 *<br> | class MainActivity
 *<br> |  ---    ___________________________
 *<br> | |(II) >|Subparteas para el tipo (III)| 
 *<br> | | T |  ------------\/----------------
 *<br> | | I |  ------------------------
 *<br> | | P | |	Materiales (IV)		|    			_______
 *<br> | | O | |						|				
 *<br> | | S | |              		    |				ZAPATO
 *<br> | |   | |						|				_______
 *<br> |  ---  --------------------------				
 *<br> | --/\--------------				-----
 *<br> | |Partes del zapato		(I)			|
 *<br> | ------------------				-----
 *<br> |__________________________________________________________
 *<br> 
 *
 *
 *
 */
public class DesignActivity extends ActionBarActivity  implements TabsBtnFragment.OnSetTabsBtnFragment {
	//	public class MainDesignActivity extends ActionBarActivity implements TabsBtnFragment.OnSetTabsBtnFragment, 
	//	GridviewFragment.OnSetListener,
	//	GridviewSectionsFragment.OnSetListener,
	//	HorizontalScrollViewFragment.OnSetGetData {


	public static String ARG_FAMILY = "shoeFamily";
	public static String ARG_PART = "shoePart";

	private static String PRESS_SEGMENT = "press_segment";
	private static String PRESS_RADIOGROUP_CIRCLE = "press_radiogroup_circle";
	private static String PRESS_PATHMENU = "press_pathmenu";
	private static String PRESS_SHOE = "press_shoe";
	
	// Tags para identificar a los fragments
	public static String TAG_TYPES_OF_SHOEPART = "tag_TypesOfShoepart";
	public static String TAG_SUBPARTS_OF_TYPE = "tag_SubpartsOfType";
	public static String TAG_MATERIALS = "tag_Materials";

//	private ModelDummy mModel;
	private ModelDummy mModelMaterials;
	private ModelDummy mModelMyMaterials;

	private int mShoeFamilyCurrent;

	private static String[] mShoeSectionsArray;


	private RadioGroup mPaletteRadioGroup;
	private RadioGroup mPaletteExtrasRadioGroup;

	private PaletteFragment mPaletteFragment;
	private PaletteSegmentTabsFragment mSegmentTabs;

	private ShoeFragment mShoeFragment;
	private View mSelectorShoepartView;
	private ProgressBar mProgressCircleView;
	private TextView mProgressTextView;
	private TextView mProgressSignTextView;
	private View mMyMaterialsView;
	private View mExtrasMenuPathView;

    private Handler mHandler = new Handler(){

    	
    	@Override
        public void handleMessage(Message msg) {
        
            View gridview;
            ArrayList<Bitmap> dataArrayList;
            View progress;

            
    		/* --------------------
    		 * Imagenes downloaded
    		   --------------------*/
            switch(msg.what){
            
            case ModelDummy.DOWNLOAD_SHOERENDER_COMPLETED:
            	
//        		gridview = (View) mViewPalette.findViewById(R.id.gridview_types);
//        		gridview.setVisibility(View.VISIBLE);

        		Bitmap shoeRender = (Bitmap) msg.obj;
        		mShoeFragment.updateShoeView(shoeRender);
//        		showTypesOfShoepartGridview (dataArrayList, mShoeFamilyCurrent, mShoepartCurrent);

//        		progress = (View) mViewPalette.findViewById(R.id.progressBar_);
//        		progress.setVisibility(View.GONE);

        		break;

            }
        }
    };

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (Utils.isTablet(getApplicationContext()))
			setContentView(R.layout.activity_design);
		else
			setContentView(R.layout.activity_design_movil);



		/* ----------------------
		 *  Conexi�n a internet
		   ---------------------*/
		String msgConnectString = null;
		if (UtilsNet.isNet(getApplicationContext()))
			msgConnectString = getResources().getString(R.string.connectNetYes);
		else
			msgConnectString = getResources().getString(R.string.connectNetNo);
		
		Toast.makeText(getApplicationContext(), msgConnectString, Toast.LENGTH_SHORT).show();

		
		/* --------------------
		 * Argumentos: Familia
		  ---------------------*/
		Intent intent = getIntent();
		mShoeFamilyCurrent = intent.getIntExtra(ARG_FAMILY, ModelDummy.FAMILIA_SALON); 
		
		
		
		/* ------
		 * Modelo
		   ------*/
//		mModel = new ModelDummy(this);
		mModelMaterials = new ModelDummy(this);
		mModelMyMaterials = new ModelDummy(this);



		/* ----------
		 * Action bar
		 ------------*/
		ActionBar bar = getSupportActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Black_transparent_black_hex_10)));
		bar.setHomeButtonEnabled(true);

		/**
		// Hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 */

		getSupportActionBar().hide();

		// Calcula tama�o de la actionBar y ajusta la altura del view para tablet.
		/**
		if (Utils.isTablet(this)) {
			LinearLayout designLayout = (LinearLayout) findViewById(R.id.design_layout);
			ActionBar actionBar = this.getSupportActionBar();
			ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) designLayout.getLayoutParams();
			TypedValue tv = new TypedValue();

			// Seg�n  appcompat o no.
			int actionBarHeight;
			if (this.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))   
				actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
			else 
				actionBarHeight = actionBar.getHeight();

			// Ajusta view por debajo de la actionBar
			params.setMargins(0, actionBarHeight, 0, 0);
			designLayout.setLayoutParams(params);
			designLayout.requestLayout();

		}
		// Hide the action bar para phone.
		else {
			getSupportActionBar().hide();
		}
		 */

		/**
        // Para mantener la StatusBar por encima de la ActionBar
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

		private void hideStatusBar() {
	        WindowManager.LayoutParams attrs = getWindow().getAttributes();
	        attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
	        getWindow().setAttributes(attrs);
	    }

	    private void showStatusBar() {
	        WindowManager.LayoutParams attrs = getWindow().getAttributes();
	        attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
	        getWindow().setAttributes(attrs);
	    }
		 */

		/**
	    if (Build.VERSION.SDK_INT < 16) {
	        // Hide the status bar
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        // Hide the action bar
	        getSupportActionBar().hide();
	    }
	    else if (Build.VERSION.SDK_INT >= 16) {
	        // Hide the status bar
	        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
	        // Hide the action bar
	        getActionBar().hide();
	    }
		 */



		/*---------------
		 * Init variables
		  ---------------*/
		//		mShoepartItemCurrent = mModel.SHOEPART_BODY;
		//		mTypeOfShoepartItemCurrent = 0;
		mShoeSectionsArray = getResources().getStringArray(R.array.ShoepartName);




		/* -------
		 * Paleta
		   -------*/
		//		refreshPallettewPageviewFragment();
		refreshPalletteFragment();

		
		
		/* --------------
		 * Mis materiales
		   --------------*/
		showMyMaterialsHorizontalScroll(mShoeFamilyCurrent, 0);

		
		
		/* --------
		 * Pesta�as
		   --------*/
		refreshSegmentTabsFragment();	//(mModel.SHOEPARTS[mModel.SHOEPART_BODY]);



		/* --------------------
		 * 'RadioGroup circle'.
		   --------------------*/
		setupRadioGroupCircleListener();



		/* ---------------------------
		 * Adornos y Extras: Menu Path
		   ---------------------------*/
		SatelliteMenu menu = (SatelliteMenu) findViewById(R.id.ornament_extras_menupath);
		mExtrasMenuPathView = (View) findViewById(R.id.ornament_extras_menupath);
		
		List<SatelliteMenuItem> items = new ArrayList<SatelliteMenuItem>();
		items.add(new SatelliteMenuItem(5, R.drawable.ico_handmade_extras_image));
		items.add(new SatelliteMenuItem(4, R.drawable.ico_handmade_adornos_cintas_image));
		items.add(new SatelliteMenuItem(3, R.drawable.ico_handmade_adornos_traseros_image));
		items.add(new SatelliteMenuItem(2, R.drawable.ico_handmade_adornos_delanteros_image));
		items.add(new SatelliteMenuItem(1, R.drawable.ico_handmade_adornos_text));
		//	        items.add(new SatelliteMenuItem(5, R.drawable.sat_item)); 
		menu.addItems(items);        
		
		/*
		 * Listener adornos y extras
		 */
		menu.setOnItemClickedListener(new SateliteClickedListener() {

			public void eventOccured(int id) {

				int shoepartIndex = menuPathToShoepart(id);
				updateTypesOfShoepart(mShoeFamilyCurrent, shoepartIndex, PRESS_PATHMENU);
			}
		});



		/* --------------
		 * Mis materiales
		   --------------*/
		mMyMaterialsView = (View) findViewById(R.id.myMaterials_layout);  //myMaterials_fragment 
		final View myMaterialsViewClickOpen = (View) findViewById(R.id.myMaterialsClickOpen_FrameLayout);
		final View myMaterialsViewClickClose = (View) findViewById(R.id.myMaterialsClickClose_FrameLayout);
		scaleMyMaterials(mMyMaterialsView, false);

		myMaterialsViewClickOpen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				myMaterialsViewClickOpen.setVisibility(View.GONE);
				myMaterialsViewClickClose.setVisibility(View.VISIBLE);
				scaleMyMaterials(mMyMaterialsView);
			}
		});


		myMaterialsViewClickClose.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				myMaterialsViewClickOpen.setVisibility(View.VISIBLE);
				myMaterialsViewClickClose.setVisibility(View.GONE);
				scaleMyMaterials(mMyMaterialsView);
			}
		});


		/*-----------------------
		 * Detalles y progreso 
		 ------------------------*/
		// ...progress circle
		 mProgressTextView = (TextView) findViewById(R.id.progressCirclePorcentTextView);
		 mProgressSignTextView = (TextView) findViewById(R.id.progressCircleSignPorcentTextView);
		 mProgressCircleView = (ProgressBar) findViewById(R.id.progressCircle);
		Typeface face = Typeface.createFromAsset(getAssets(),
				"fonts/denial.ttf");
		mProgressTextView.setTypeface(face);
		mProgressSignTextView.setTypeface(face);
		

		// ...details
		BuyDetailsFragment details = (BuyDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.buyDetails_fragment);
		details.setOnClickListener(new BuyDetailsFragment.OnClickListener() {


			@Override
			public void onClickDetails(boolean isDetailsVisible) {
				
				// Que no quede tapado el menu path
				if (isDetailsVisible)
					myMaterialsOver();
				else
					myMaterialsUnder();

			}

			@Override
			public void onClickBuy() {
				Intent intent = new Intent(getApplicationContext(), BuyActivity.class);
				startActivity(intent);

			}
		});



		/* ---------------------------------------------
		 *  Zapato (imagen, panning,...)
		   ---------------------------------------------*/
		mSelectorShoepartView = (View) findViewById(R.id.selectorImageView);
		// Situaci�n sobre el cuerpo del zapato
		Utils.moveViewWithMargin(getApplicationContext(), mSelectorShoepartView, 900, 470); 
//		selectorShoepartVisibility(false);
		
		mShoeFragment = (ShoeFragment)getSupportFragmentManager()
				.findFragmentById(R.id.shoe_fragment);
		mShoeFragment.setOnListener(new ShoeFragment.OnListener() {

			@Override
			public void onClickShoe(int shoepartIndex) {
				updateTypesOfShoepart(mShoeFamilyCurrent, shoepartIndex, PRESS_SHOE);
			}

			@Override
			public Bitmap onGetShoeRender(int sequenceAngle) {
				return mModelMaterials.loadShoeRender(
						mShoeFamilyCurrent, 
						mPaletteFragment.getShoepartCurrent(), 
						mPaletteFragment.getTypePositionCurrent(),
						mPaletteFragment.getSubpartPositionCurrent(),
						mPaletteFragment.getMaterialsSectionCurrent(),
						mPaletteFragment.getMaterialsPositionCurrent(), 
						sequenceAngle);
			}

//			@Override
//			public void onGetShoeRender(int shoepartIndex, int typeIndex, int subpartsIndex, 
//					int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle) {
			@Override
				public Bitmap onGetShoeRender(int shoepartIndex, int typeIndex, int subpartsIndex, 
						int materialsSectionIndex, int materialsPositonIndex, int sequenceAngle) {
				
//		    	int tagHandleCompleted = ModelDummy.DOWNLOAD_SUBPARTS_COMPLETED;
//		    	int idView = R.id.subparts_layout;
//		    	int idProgress = R.id.progressBar_subparts;
//				
////				if (aTypeOfShoepartIndex%2 == 0)
//					apiConnect("http://hidden-reef-8347.herokuapp.com/api/subparts0/",0, idProgress, tagHandleCompleted);

				return mModelMaterials.loadShoeRender(
						mShoeFamilyCurrent, 
						shoepartIndex, typeIndex, subpartsIndex, 
						materialsSectionIndex, materialsPositonIndex,  
						sequenceAngle);
			
			}
			
				public Bitmap onGetShoeRender(Bitmap shoeRender) {
				
//				return mModelMaterials.loadShoeRender(
//						mShoeFamilyCurrent, 
//						shoepartIndex, typeIndex, subpartsIndex, 
//						materialsSectionIndex, materialsPositonIndex,  
//						sequenceAngle);
					return shoeRender;

			}

			    public void apiConnect (final String urlString, int idView, int idProgressBar, final int tagHandlerCompleted) {


					/*
					 * Datos
					 */
//					ArrayList<Bitmap> data = new ModelDummy(mContext).loadTypesOfShoepart(getActivity(), aShoeFamilyIndex, aShoePartIndex);
					// test api
//			    	View view = null;
//					if (idView != 0) {
//						view = (View) mViewPalette.findViewById(idView);
//						view.setVisibility(View.INVISIBLE);
//					}
//					
//					View progress = (View) mViewPalette.findViewById(idProgressBar);
//					progress.setVisibility(View.VISIBLE);
					
					final ModelDummy model = new ModelDummy(getApplicationContext());
					new Thread(new Runnable() {

						@Override
						public void run() {

							boolean isNet = model.apiConnect(urlString);
							ArrayList<Bitmap> shoeRenderBitmap = model.getSection(0);

							// Alerta final hilo
							Message msg = Message.obtain();
							msg.what = tagHandlerCompleted;
							msg.obj = shoeRenderBitmap.get(0);
							msg.arg1 = isNet ? 1 : 0;
							mHandler.sendMessage(msg);

						}
					}).start();


					// Llamada a gridview con los datos
//					showTypesOfShoepartGridview (dataArrayList, aShoeFamilyIndex, aShoePartIndex);

				}

				
			@Override
			public void onClickSelector(float xScreen, float yScreen) {

				Utils.moveViewWithMargin(getApplicationContext(), mSelectorShoepartView, (int)xScreen, (int)yScreen); 
			}

			@Override
			public void onClickSelectorVisibility(boolean visibility) {
				selectorShoepartVisibility(visibility);				
			}


		});

		//		      PalettePageviewFragment frag = (PalettePageviewFragment) getSupportFragmentManager().findFragmentById(R.id.palettepageview_fragment);
		//		      frag.refreshFragmentTypeOfShoepart();

		mShoeFamilyCurrent = ModelDummy.FAMILIA_SALON;




		/*--------------------------------------------------------
		 * Listener para mostrar la Actionbar y la barra inferior.
		  --------------------------------------------------------*/
		/**
		setupScreenListener();
		*/
		/**
		setupShoeListener(R.id.buttonPuntera41, TYPE_OF_SHOEPART_TOECAP);
		setupShoeListener(R.id.buttonPuntera51, TYPE_OF_SHOEPART_TOECAP);
		setupShoeListener(R.id.buttonTrasera13, TYPE_OF_SHOEPART_BACK);
		setupShoeListener(R.id.buttonTrasera23, TYPE_OF_SHOEPART_BACK);
		 */



		/* -------------------
		 * Recoge excepciones.
		  -------------------*/
		/**
		Thread.setDefaultUncaughtExceptionHandler(
				new UncaughtExceptionHandler() {

					@Override
					public void uncaughtException(Thread thread, Throwable ex) {
						Log.e("(JMF)Error", "Unhandled exception: " + ex.getMessage());
						Toast.makeText(getApplicationContext(), R.string.app_fatalError, Toast.LENGTH_LONG).show();
						System.exit(1);
					}
				});

		 */


	} /* Fin onCreate */




	/** @see.................................
	 *<br> 
	 *<br> Pulsaciones sobre Action bar.
	 *	-> Home' del 'ActionBar' cambia el backgroug
	 *<br> 
	 *<br> @param item	MenuItem
	 *<br> @author Jose Manuel Fierro, 2014.
	 *<br> 
	  @see...................................*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) {
		case android.R.id.home: 
			//	        	onBackPressed();
			/* -----------------------------------------------------------------------------------------
			 * Al pulsar sobre el 'Home' del 'ActionBar' cambia el backgroug en las sucesivas versiones.
	        	   ------------------------------------------------------------------------------------------*/
			backgourdSkinChange();
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}



	private void backgourdSkinChange() {
		ImageView back = (ImageView) findViewById(R.id.backgroud);
		String backString = (String) back.getTag();
		if (backString == null || backString == "001") {
			back.setTag("002");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud2));
		}
		else if (backString == "002") {
			back.setTag("003");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud3));
		}
		else if (backString == "003") {
			back.setTag("003b");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud3b));
		}
		else if (backString == "003b") {
			back.setTag("003c");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud3c));
		}
		else if (backString == "003c") {
			back.setTag("004");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud4));
		}
		else if (backString == "004") {
			back.setTag("005");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud5));
		}
		else if (backString == "005") {
			back.setTag("006");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud6));
		}
		else if (backString == "006") {
			back.setTag("Nada");
			back.setVisibility(View.INVISIBLE);
		}
		else {
			back.setTag("001");
			back.setImageDrawable(getResources().getDrawable(R.drawable.backgroud1));
			back.setVisibility(View.VISIBLE);
		}


	}





	/**
	public void refreshPallettewPageviewFragment () {

		FragmentManager frgManager = getSupportFragmentManager();
		FragmentTransaction frgTrans = frgManager.beginTransaction();

		PalettePageviewFragment typesOfShoepartFragment = new PalettePageviewFragment().newInstance(null);

		frgTrans.replace(R.id.palette_fragment, typesOfShoepartFragment);
		frgTrans.commit(); 

	}
	*/


	/** .........................................................
	 *<br> 
	 *<br> Fragmento para la 'Paleta': Tipos, Subpartes y Materiales
	 *<br> 
	  ...........................................................*/
	public void refreshPalletteFragment () {

		FragmentManager frgManager = getSupportFragmentManager();
		FragmentTransaction frgTrans = frgManager.beginTransaction();

		mPaletteFragment = new PaletteFragment();

		frgTrans.replace(R.id.palette_fragment, mPaletteFragment);
		frgTrans.commit(); 

		
		
		
		/* ---------------------
		 * Listener item paleta
		   ---------------------*/
		mPaletteFragment.setOnClickListener(new PaletteFragment.OnClickListener() {
			
			@Override
			public void onSetModel(ModelDummy model) {
				mModelMaterials = model;
				
			}
			
			@Override
			public void onClickItemMaterials(int shoepartIndex, int typeIndex,
					int subpartsIndex, int materialsSectionIndex, int materialsIndex) {

				ShoeFragment shoeFrag = (ShoeFragment) getSupportFragmentManager().findFragmentById(R.id.shoe_fragment);
				shoeFrag.updateShoeView();

				mModelMaterials.loadShoeRender(
						mShoeFamilyCurrent, 
						shoepartIndex, typeIndex, subpartsIndex, 
						materialsSectionIndex, materialsIndex,  
						shoeFrag.mShoeAngleCurrent);

				
				mModelMyMaterials.addItemFromModel(mModelMaterials, materialsSectionIndex, materialsIndex);
				scaleMyMaterialsInflate();
				showMyMaterialsHorizontalScroll(mShoeFamilyCurrent, 0);
				
				
				updateProgress();
				
			}
		});
		
		
	}


	private void updateProgress() {

		// Progress bar
		PaletteFragment paletteFrag = (PaletteFragment) getSupportFragmentManager().findFragmentById(R.id.palette_fragment);
		BuyDetailsFragment buyDetailsFrag = (BuyDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.buyDetails_fragment);
		
		
		int porcent = 0;
		if (paletteFrag.getShoepartCurrent() == ModelDummy.SHOEPART_BODY)
			porcent = 25;
		else if (paletteFrag.getShoepartCurrent() == ModelDummy.SHOEPART_TOECAP)
			porcent = 50;
		else if (paletteFrag.getShoepartCurrent() == ModelDummy.SHOEPART_BACK)
			porcent = 75;
		else if (paletteFrag.getShoepartCurrent() == ModelDummy.SHOEPART_HEEL)
			porcent = 100;
		
		if (porcent > 0) {
			
			mProgressTextView.setText(" "+porcent);
			mProgressTextView.setTextColor(getResources().getColor(R.color.MadeInMeDarck));
			mProgressSignTextView.setTextColor(getResources().getColor(R.color.MadeInMeDarck));

			mProgressCircleView.setProgress(porcent);
			buyDetailsFrag.updateProgress(porcent);
		}
		

	}


	public void refreshFragmentTypeOfShoepart () {
		//		      PalettePageviewFragment frag = (PalettePageviewFragment) getSupportFragmentManager().findFragmentById(R.id.palettepageview_fragment);
		//		      frag.refreshFragmentTypeOfShoepart();
	}


	/** ....................................
	 *<br> 
	 *<br> Fragmento para las pesta�as: 
	 *<br> Cuerpo, Punta, Trasera y  Tac�n.
	 *<br> 
	  ......................................*/
	private void refreshSegmentTabsFragment () {

		FragmentManager frgManager = getSupportFragmentManager();
		FragmentTransaction frgTrans = frgManager.beginTransaction();

		mSegmentTabs = new PaletteSegmentTabsFragment();
		//		PaletteSegmentTabsFragment segmentFragment = new PaletteSegmentTabsFragment();

		frgTrans.replace(R.id.segment_fragment, mSegmentTabs);
		frgTrans.commit();

		//		segmentFragment.setRadioGroupTextEnable(position);

		/* ---------
		 * Listener
		  ----------*/
		mSegmentTabs.setOnClickListener(new PaletteSegmentTabsFragment.OnClickListener() {

			@Override
			public void onItemClick(int position) {
				updateTypesOfShoepart(mShoeFamilyCurrent, position, PRESS_SEGMENT);
			}
		});
	}


	/** ...................................
	 *<br> 
	 *<br>  Actualiza la posicion del pageview
	 *<br>  
	 *<br> @param position
	 *
	 *<br> @author Jose Manuel Fierro, 2014.
	 *<br> 
	    .......................................*/
	/**
	public void pageviewItemEnable (int position) {
		PalettePageviewFragment frag = (PalettePageviewFragment) getSupportFragmentManager().findFragmentById(R.id.palette_fragment);
		frag.setCurrentPage(position);

	}
	*/


	/** ............................................................................................
	 *<br> 
	 *<br>  Devuelve la secci�n del zapato dada una parte del zapato
	 *<br>  
	 *<br>  @param shoePart		1,2,...(Cuerpo:mModel.SHOEPART_BODY, Puntera:mModel.SHOEPART_TOECAP, ...)
	 *<br>  @return
	 *<br>  
	 *<br> @author Jose Manuel Fierro, 2014.
	 *<br>  
	   .............................................................................................*/
	public String getShoeSection(int shoePart) {

		if (shoePart > mShoeSectionsArray.length)
			return null;

		return mShoeSectionsArray[shoePart];
	}





	/** .................................................................
	 *<br> 
	 *<br> 'RadioGroup circle'
	 *<br> 	-> Listener click.
	 *<br> @author Jose Manuel Fierro, 2014.
	 *<br> 
	   ..................................................................*/
	private void setupRadioGroupCircleListener () {

		mPaletteRadioGroup = (RadioGroup) findViewById(R.id.circle_radioGroup);
		mPaletteExtrasRadioGroup = (RadioGroup) findViewById(R.id.circle_extras_radioGroup);
		
		mPaletteRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup v, int id) {

				// S�lo actualiza 'Types' si se ha tocado el RadioGroup...
				if (id != -1) {
					RadioButton checkedRadioButton = (RadioButton)v.findViewById(id);
					boolean isChecked = checkedRadioButton.isChecked();
					if (isChecked) {
						View radioButton = v.findViewById(id);
						if (radioButton != null) {
							int idx = v.indexOfChild(radioButton);
							
							// ... actualizaci�n de 'Types'
							updateTypesOfShoepart(mShoeFamilyCurrent, idx, PRESS_RADIOGROUP_CIRCLE);
						}
					}
				}
			}
		});
	}


	/** ....................................
	 *<br> 
	 *<br> Listener click screen:
	 *<br> 
	 *<br> 	-> Hide/Show HActionbar.
	 *<br> 	-> Hide/Show SlidingDrawerCustom.
	 *<br> @author Jose Manuel Fierro, 2014.
	 *<br>  
	  .......................................*/
//	private void setupScreenListener () {
//
//		View screenView = findViewById(R.id.screenDesign);
//
//		//		View tabsFragments = findViewById(R.id.slidingDrawerCustom);
//		//		tabsFragments.setVisibility(View.GONE);
//
//		// Listener
//		screenView.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//				/* ---------------------
//				 * Hide/Show HActionbar.
//				   ---------------------*/
//				/**
//				if (getSupportActionBar().isShowing()) {
//					// Hide the action bar
//					getSupportActionBar().hide();
//
//				} else {
//					// Show the action bar
//					getSupportActionBar().show();
//
//					//					Animation animation = AnimationUtils.loadAnimation(v.getContext(), R.anim.translate_down);
//					//					tabsView.startAnimation(animation);
//					//					tabsView.setVisibility(View.INVISIBLE);
//
//				}
//				 */
//
//			}
//		});
//	}	





	/** ..................................................
	 *<br> 
	 *<br> Actualiza los 'tipos' correspondientes a shoepart  
	 *<br> y tambien las 'vistas' asociadas: T�tulo, Pesta�as, etc. 
	 *<br> 
	 *<br> @param shoepartIndex	Parte del zapato.
	 *<br> @author Jose Manuel Fierro, 2014.
	 *<br> 
	 .....................................................*/
	public void updateTypesOfShoepart (int shoeFamilyIndex, int shoepartIndex, String isPress) {

		if (shoepartIndex != -1) {
			// Palette
			mPaletteFragment.updateTitle(shoepartIndex);
			mPaletteFragment.destroyAndShowGridviewTypesOfShoepart(shoeFamilyIndex, shoepartIndex);
			mPaletteFragment.destroySubpartsView();
			mPaletteFragment.destroyMaterialsView();

			// Segment
			mSegmentTabs.setCheck(shoepartIndex);

			// Circle button
			if (shoepartIndex < 4)
				checkRadiogroupCircle(shoepartIndex);
			else {
				mPaletteRadioGroup.clearCheck();
			//			checkExtrasRadiogroupCircle(shoepartIndex);
			} 
			
			// Deselecciona zapato salvo que venga desde tocar la imagen del zapato
			if (isPress == PRESS_SHOE)
				selectorShoepartVisibility(true);
			else	
				selectorShoepartVisibility(false);
		}

	}



	/** ....................................................
	 *<br> 
	 *<br> Selecciona un button radio circle por posici�n.
	 *<br> 
	   .....................................................*/
	public void checkRadiogroupCircle (int position) {

		int numSegment = mPaletteRadioGroup.getChildCount();

		if (position >= 0 && position < numSegment) {
			RadioButton radioButton = (RadioButton) mPaletteRadioGroup.getChildAt(position);
			radioButton.setChecked(true);
		} 
	}



	/** ....................................................................
	 *<br> 
	 *<br> Obtiene el ShoePartIndex partiendo del menu path (Adrornos y Extras)
	 *<br> 
	 *<br> @param id:	posici�n del menu path
	 *<br> @return
	 *<br> 
	   .....................................................................*/
	private int menuPathToShoepart (int id){

		int shoepartIndex = 0;
		
		if (id==1 || id==2)
			shoepartIndex = 4;
		else if (id==3)
			shoepartIndex = 5;
		else if (id==4)
			shoepartIndex = 6;
		else if (id==5)
			shoepartIndex = 7;

		return shoepartIndex;

	}
	private int shoepartToExtras (int shoepartIndex){

		int id = shoepartIndex-4;

		return shoepartIndex;

	}

	@Override
	public void onTabsBtnItemChanged(int position) {
		// TODO Auto-generated method stub

	}




	/** ..........................................................
	 *<br> 
	 *<br> Muestra las subpartes de un tipo de una parte del zapato
	 *<br> 
	   ...........................................................*/
	private void showMyMaterialsHorizontalScroll (int shoeFamily, int position) {

		ArrayList<Bitmap> data = new ArrayList<Bitmap>();
		data = mModelMyMaterials.loadMyMaterials(mShoeFamilyCurrent);


		View myMaterialsView = (View) this.findViewById(R.id.myMaterials_fragment);
		//		Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale_fromcenter);
		//		mShoeSubpartsView.startAnimation(anim);
//		myMaterialsView.setVisibility(View.VISIBLE);

		JMFHorizontalScrollGallery horizScroll = new JMFHorizontalScrollGallery(
				this, myMaterialsView, 
				R.layout.gallery_horizontalscroll_item, 
				data);

		horizScroll.setOnClickListener(new JMFHorizontalScrollGallery.OnClickListener() {

			@Override
			public void onClickItem(int position) {

			}
		});

	}

	/** .................................
	 *<br> 
	 *<br> Scala el view de 'Mis Materiales'
	 *<br> @param v
	 *<br> 
	   ..................................*/
	private void scaleMyMaterials (View v) {

		Integer isScaleInteger = (Integer) v.getTag();
		boolean isScale = (isScaleInteger == null || isScaleInteger == 0);
		scaleMyMaterials (v, isScale);

	}

	private void scaleMyMaterials (View v, boolean isScale) {

		Animation anim;
		View frag = v.findViewById(R.id.myMaterials_fragment);
//		View line = v.findViewById(R.id.myMaterialsLineView);
		if (isScale) {

			myMaterialsOver();
			
			anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_out_fromrightcenter);
//			frag.setBackgroundDrawable(getResources().getDrawable(R.drawable.abc_menu_dropdown_panel_holo_dark));
			//			line.setVisibility(View.GONE);
			v.setTag(1);
		} 
		else {

			// Mis materiales se ponen atras para que no interfiera la paleta.
			myMaterialsUnder();

			anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_in_fromrightcenter);
			frag.setBackgroundDrawable(getResources().getDrawable(R.drawable.abc_ic_clear));
			//			line.setVisibility(View.VISIBLE);
			//			v.setBackgroundColor(Color.parseColor("#80000000"));
			v.setTag(0);
		}

		anim.setFillEnabled(true);
		anim.setFillAfter(true);
		//				frag.setAnimation(anim);
		v.startAnimation(anim);

		//				v.setScaleX(0.5f);
		//				v.setScaleY(0.5f);

	}

	private void scaleMyMaterialsInflate () {
//		View fragView = (View) findViewById(R.id.myMaterials_fragment);
//		View v = (View) findViewById(R.id.myMaterials_layout);
//		v.setVisibility(View.GONE);
		
		Integer isScaleInteger = (Integer) mMyMaterialsView.getTag();
		boolean isScale = (isScaleInteger == null || isScaleInteger == 0);
		if (isScale) {
			scaleMyMaterials (mMyMaterialsView);
			scaleMyMaterials (mMyMaterialsView);
		}

//		v.setVisibility(View.VISIBLE);

	}


	// Mis materiales se ponen delante para que sean accesibles.
	private void myMaterialsOver() {
		
//		v.bringToFront();
		View materialsView = (View) findViewById(R.id.mymaterials_progress_buy_datails_layout);
		materialsView.bringToFront();
		mExtrasMenuPathView.bringToFront();
		
	}

	
	// Mis materiales se ponen atras para que no interfiera con otros views (e.- paleta).
	private void myMaterialsUnder() {
		
//		Utils.sendViewToBack(v);
		View palleteView = (View) findViewById(R.id.Pallete_and_shoe_layout);  //palette_layout); 	//palette_fragment);
		palleteView.bringToFront();
		mExtrasMenuPathView.bringToFront();

	}

	
	
	private void selectorShoepartVisibility(boolean visibility) {
		if (visibility)
			mSelectorShoepartView.setVisibility(View.VISIBLE);
		else
			mSelectorShoepartView.setVisibility(View.GONE);				

	}
	
	
}

