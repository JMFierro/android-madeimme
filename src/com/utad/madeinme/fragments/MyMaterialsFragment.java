package com.utad.madeinme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.utad.madeinme.R;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */


public class MyMaterialsFragment extends Fragment {

	
	/* --------------
	 * Listener click
	   --------------*/
	private OnClickListener mListener;
	public interface OnClickListener {
		public void onClickItem();
	}
	public void setOnClickListener(OnClickListener listener) {
		this.mListener = listener;
	}

	
	
	public static final String ARG_SECTION_NUMBER = "section_number";
	private View mView;
	private boolean mIsDetailsVisible;

	public MyMaterialsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.my_materiales_fragment, container, false);

		mView = rootView;
		
		return rootView;
	}


	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);

		
	}

	
}
