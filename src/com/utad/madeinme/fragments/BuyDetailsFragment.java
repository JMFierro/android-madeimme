package com.utad.madeinme.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.utad.madeinme.R;
import com.utad.madeinme.utils.Utils;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 * Muestra en varias l�neas el Precio, bot�n de compra, boton de detalles y progreso.
 *
 * ---------------------------------------------
 * M�todo p�blico 'updateProgress(int porcent)'
 * ---------------------------------------------
 * 
 */


public class BuyDetailsFragment extends Fragment {

	/* --------------
	 * Listener click
	   --------------*/
	private OnClickListener mListener;
	public interface OnClickListener {
		public void onClickDetails(boolean isDetailsVisible);
		public void onClickBuy();
	}
	public void setOnClickListener(OnClickListener listener) {
		this.mListener = listener;
	}



	public static final String ARG_SECTION_NUMBER = "section_number";
	private View mView;
	private boolean mIsDetailsVisible;

	public BuyDetailsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.buy_details_progress, container, false);

		mView = rootView;

		setup();
		
		/* ----------------
		 * Listener Compra
		   ---------------*/
		Button buyButtons = (Button) mView.findViewById(R.id.buyButton);
		buyButtons.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mListener != null)
					mListener.onClickBuy();

				//				Intent intent = new Intent(getActivity(), BuyActivity.class);
				//				startActivity(intent);

			}
		});

		/* ----------------
		 * Listener Details
		  ------------------*/
		mIsDetailsVisible = false;
		LinearLayout showDetails = (LinearLayout) mView.findViewById(R.id.detailsText_layout);
		showDetails.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				//				if (mListener != null)
				//					mListener.onClickItem(mIsDetailsVisible);
				LinearLayout detailsContent = (LinearLayout) mView.findViewById(R.id.detailsContent_layout);
				TextView detailsTextView = (TextView) mView.findViewById(R.id.detailsTextView);

				// Que no quede tapado el menu path
				if (mListener != null)
					mListener.onClickDetails(!mIsDetailsVisible);

				if (mIsDetailsVisible) {
					detailsTextView.setText(getActivity().getResources().getString(R.string.detailsShow));
					mIsDetailsVisible = false;
					detailsContent.setVisibility(View.GONE);
				}
				else {
					detailsTextView.setText(getActivity().getResources().getString(R.string.detailsHide));
					mIsDetailsVisible = true;
					detailsContent.setVisibility(View.VISIBLE);
				}

			}
		});

		//		showDetails.setOnLongClickListener(new View.OnLongClickListener() {
		//			
		//			@Override
		//			public boolean onLongClick(View v) {
		//			    LinearLayout rowSecond = (LinearLayout) mView.findViewById(R.id.row_second);
		//			    rowSecond.setVisibility(View.GONE);
		//			    setup();
		//				return false;
		//			}
		//		});

		return rootView;
	}


	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);



	}

	
	
	/** ...........................................................
	 *
	 * Asigna la fuente 'Georgi.tff' para el n�mero del precio.
	 * 
	 * Posiciona las divisiones entre l�neas haciendo uso del 'heigh' 
	 * de cada una de ellas.
	 * 
	 ................................................................*/
	private void setup() {


		/* ---------------
		 *  Precio: fuente
			   ---------------*/
		TextView priceTextView = (TextView) mView.findViewById(R.id.priceBuyTextView);
		Typeface face = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/georgia.ttf");

		priceTextView.setTypeface(face);


		/* ---------------------------------------
		 * Posiciona las divisiones entre l�neas.
         -----------------------------------------*/
		// Width y Height primera fila
		LinearLayout rowFirstView = (LinearLayout) mView.findViewById(R.id.row_first);
		rowFirstView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int wPrice = rowFirstView.getMeasuredWidth();
		int hRowFirst = rowFirstView.getMeasuredHeight();
		// Margin
		LinearLayout.LayoutParams rowFirstLL = (LinearLayout.LayoutParams) rowFirstView.getLayoutParams();
		int marginRowFirst = rowFirstLL.bottomMargin + rowFirstLL.topMargin;

		ViewGroup.MarginLayoutParams vlp = (ViewGroup.MarginLayoutParams) rowFirstView.getLayoutParams();
		marginRowFirst = rowFirstLL.bottomMargin + rowFirstLL.topMargin;


		// Height segunda fila
		LinearLayout rowSecondView = (LinearLayout) mView.findViewById(R.id.row_second);
		rowSecondView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int hRowSecond = rowSecondView.getMeasuredHeight();
		// Margin
		LinearLayout.LayoutParams rowSecondLL = (LinearLayout.LayoutParams) rowSecondView.getLayoutParams();
		int marginRowSecond = rowSecondLL.bottomMargin + rowSecondLL.topMargin;


		// Margen top & Width Division 1
		TextView divTextView1 = (TextView) mView.findViewById(R.id.division1);
		divTextView1.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int hTextView1 = divTextView1.getMeasuredHeight();
		divTextView1.setWidth(wPrice);

		int margin1 = (int) ((int) hRowFirst-(hTextView1*0.5));
		Utils.setMarginView(divTextView1, 0, margin1, 0, 0);
		

		// Margen top & Width Division 2
		TextView divTextView2 = (TextView) mView.findViewById(R.id.division2);
		divTextView2.setWidth(wPrice);

		int margin2 = (int) (hRowFirst + hRowSecond + marginRowFirst + marginRowSecond);   //(int) ((int) (h1 + h2)-(hTextView2*0.5));
		Utils.setMarginView(divTextView2, 0, margin2, 0, 0);

	}
	

	public void updateProgress(int porcent) {
		
		ProgressBar progressBar = (ProgressBar) mView.findViewById(R.id.progressBar);
		progressBar.setProgress(porcent);
		
		TextView porcentTextView = (TextView) mView.findViewById(R.id.progressBarPorcentTextView);
		porcentTextView.setText(porcent+"%");
	}

}
