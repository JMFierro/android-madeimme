package com.utad.madeinme.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;

import com.utad.madeinme.R;
import com.utad.madeinme.jmf.model.ModelDummy;
import com.utad.madeinme.utils.MarginProxy;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 */
public class TabsBtnFragment extends Fragment {
	
	private boolean isUp;
    public static int TAB_BUTTON_ON = 1;
    
    public static ArrayList<String> mPartShoes;
    
    private Map<Integer, Object> mTabsBtnAttach = new HashMap<Integer, Object>();
    

	/*------------------------------------
	 *  Jose Manuel Fierro Conchouso, 2014.
	 * Callback
	 --------------------------------------*/
	private OnSetTabsBtnFragment mCallback  = CallbackVoid;

	public interface OnSetTabsBtnFragment {
		public void onTabsBtnItemChanged(int position);
	}

	private static OnSetTabsBtnFragment CallbackVoid = new OnSetTabsBtnFragment() {
		@Override
		public void onTabsBtnItemChanged(int position) {
		}
	};

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		isUp = true;

		mPartShoes = new ArrayList<String>();
		
		mPartShoes.add(getActivity().getString(R.string.ShoePart1).toUpperCase());
		mPartShoes.add(getActivity().getString(R.string.ShoePart2).toUpperCase());
		mPartShoes.add(getActivity().getString(R.string.ShoePart3).toUpperCase());
		mPartShoes.add(getActivity().getString(R.string.ShoePart4).toUpperCase());
		mPartShoes.add(getActivity().getString(R.string.ShoePart5).toUpperCase());
		mPartShoes.add(getActivity().getString(R.string.ShoePart6).toUpperCase());

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.tabsbtn_parts, container, false);
//		return Utils.rotation(view, 360.0f);
		
		// Orientacion de las pesta�as (up/down).
		if (isUp)
			return inflater.inflate(R.layout.tabsbtn_parts, container, false);
		else
			return inflater.inflate(R.layout.tabsbtn_parts_down, container, false);
	}

	
	
    /**................................................
     * 
     * 'TABS' en la 'ActionBars' 
     * ** Jose Manuel Fierro Conchouso, 2014 **
     ..................................................*/
    //La vista ha sido creada y cualquier configuraci�n guardada est� cargada
    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        
        
		/*----------------------
		 * Tabs inicializaci�n.
		 -----------------------*/
        // Botones listen, se asocian con las pesta�as & se activa el listener.
        setTabBtnAttachListener(R.id.tab_button1, ModelDummy.SHOEPART_BODY);
        setTabBtnAttachListener(R.id.tab_button2, ModelDummy.SHOEPART_TOECAP);
        setTabBtnAttachListener(R.id.tab_button3, ModelDummy.SHOEPART_BACK);
        setTabBtnAttachListener(R.id.tab_button4, ModelDummy.SHOEPART_HEEL);
        setTabBtnAttachListener(R.id.tab_button5, ModelDummy.SHOEPART_ORNAMENT_BACK);
        setTabBtnAttachListener(R.id.tab_button6, ModelDummy.SHOEPART_EXTRAS);
        
        // boton on
        setTabBtnState(R.id.tab_button1, TAB_BUTTON_ON);
        
    }
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback  = CallbackVoid = (OnSetTabsBtnFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTabsBtnFragment()");
        }
    }
    
    

    /** ...................................................................
     * 
     *  Asocia un bot�n a una pesta�as.
     *  Escucha de botones y hace la llamada 'callback'. 
     *  ** Jose Manuel Fierro Conchouso, 2014. **
     ......................................................................*/
    private void setTabBtnAttachListener(int btnId, final int position) {
    	
    	// Asocia el boton a una posicion
    	mTabsBtnAttach.put(position, btnId);
    	
        Button btn = (Button) getActivity().findViewById(btnId);
        btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallback.onTabsBtnItemChanged(position);
				setTabBtnState(v, TAB_BUTTON_ON);
			}
		});

    }

    public View getTabBtnView(int position) {
    	
    	Integer btnIdinteger = (Integer) mTabsBtnAttach.get(position);
        Button btnView = (Button) getActivity().findViewById(btnIdinteger);
        return btnView;
    }

    
    /**............................................
     * 
     *  Pesta�a en estado 'On'
     *  ** Jose Manuel Fierro Conchouso, 2014. **
     .............................................*/
    public void setTabBtnState(int BtnId, int state) {
    	Button btnView = (Button) getActivity().findViewById(BtnId);
    	setTabBtnState(btnView, state); 
    }
    
    public void setTabBtnState(View v, int state) {
    	

    	/*-----------------------------------------------------------------------------
    	 *  1. Coloca resto de las tabs antes de hacer 'On' en la tab seleccionada (2).
    	 *  
    	 *  Oculta el margen de los botones.
    	 ------------------------------------------------------------------------------*/
		int margen=25, width = LayoutParams.WRAP_CONTENT; 
		int height = LayoutParams.WRAP_CONTENT; 
		int backgroudDrawableId;
		
		// isUp orientaci�n de las pesta�as hacia arriba. 
		if (isUp) 
			backgroudDrawableId = R.drawable.button_round_shape_off;
		
		// isUp orientaci�n de las pesta�as hacia abajo.
		else 
			backgroudDrawableId = R.drawable.button_round_shape_off_down;
		
		// botones off
		setTabBtn(R.id.tab_button1, margen, width, height, backgroudDrawableId, isUp);
		setTabBtn(R.id.tab_button2, margen, width, height, backgroudDrawableId, isUp);
		setTabBtn(R.id.tab_button3, margen, width, height, backgroudDrawableId, isUp);
		setTabBtn(R.id.tab_button4, margen, width, height, backgroudDrawableId, isUp);
		setTabBtn(R.id.tab_button5, margen, width, height, backgroudDrawableId, isUp);
		setTabBtn(R.id.tab_button6, margen, width, height, backgroudDrawableId, isUp);

		/*------------------------------------------------------------------------
		 *  2. 'On' en tab seleccionada despues de colocar el resto de las tab (1).
		 --------------------------------------------------------------------------*/
    	MarginProxy mp = new MarginProxy(v); 
    	mp.setHeight(100);
    	int mg = 5;
    	
		// isUp orientaci�n de las pesta�as hacia arriba.
    	if (isUp && state == TAB_BUTTON_ON) {
    		v.setBackgroundResource(R.drawable.button_round_shape_on);
        	mp.setTopMargin(mg);
    	}

		// isUp orientaci�n de las pesta�as hacia abajo.
    	else if (!isUp && state == TAB_BUTTON_ON) {
    		v.setBackgroundResource(R.drawable.button_round_shape_on_down);
        	mp.setBottomMargin(mg);
    	}
    }

    
    
    

    
    /**............................................
     * 
     *  Configura parametros del boton 'btnId'
     *  ** Jose Manuel Fierro Conchouso, 2014. **
     .............................................*/
    private void setTabBtn(int btnId, int margenLeft, int margenRigh, int margen, int width, int height, int drawableId){
    	
		MarginProxy mp = new MarginProxy(getActivity().findViewById(btnId));

		mp.setLeftMargin(margenLeft);
		mp.setRightMargin(margenRigh);
		
		setTabBtn(btnId, margen, width, height, drawableId, isUp);
    }
    
    private void setTabBtn(int btnId, int margen, int width, int height, int drawableId, boolean isUp){
    	
		MarginProxy mp = new MarginProxy(getActivity().findViewById(btnId));
		
		if (isUp)
			mp.setTopMargin(margen);
		else
			mp.setBottomMargin(margen);
		
		mp.setWidth(width);
		mp.setHeight(height);
		if (isUp) {
			if (getActivity().findViewById(btnId).getBackground().getConstantState() 
					== getActivity().getResources().getDrawable(R.drawable.button_round_shape_on).getConstantState())
				getActivity().findViewById(btnId).setBackgroundResource(R.drawable.button_round_shape_enable);
		}
		else {
			if (getActivity().findViewById(btnId).getBackground().getConstantState() 
					== getActivity().getResources().getDrawable(R.drawable.button_round_shape_on_down).getConstantState())
				getActivity().findViewById(btnId).setBackgroundResource(R.drawable.button_round_shape_enable_down);
		}
    }
}
