
package com.utad.madeinme.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utad.madeinme.DesignActivity;
import com.utad.madeinme.R;
import com.utad.madeinme.jmf.gallery.horizontal.JMFHorizontalScrollGallery;
import com.utad.madeinme.jmf.gridview.GridviewAdapter;
import com.utad.madeinme.jmf.gridview.sections.JMFGridviewWithSections;
import com.utad.madeinme.jmf.model.ModelDummy;
import com.utad.madeinme.utils.Utils;
import com.utad.madeinme.utils.UtilsNet;

/**
 * <br> Jose Manuel Fierro Conchouso, 2014
 * <br>
 * <br> Pr�ctica MadeInMe
 * <br> Master Desarrollo de apps para smartphone y tablet
 * <br> U-Tad
 * <br> 
 * <br> Fragmento que visualiza la paleta(Types, Subpartes y Materiales).
 * <br> 
 * <br> 
 * <br>  ------
 <br>	 Modelo
 <br>	 ------
 <br>
 <br>		  El Modelo contiene los datos de un gridView o Gallery, 
 <br>	    para ello utiliza un HashMap.
<br>
 <br>	    	-> private LinkedHashMap<String, ArrayList<Bitmap>> mDataHashMap = new LinkedHashMap<String, ArrayList<Bitmap>>();
<br>
 <br>	    Por ejemplo para el gridview de los 'tipos' en PaletteFragment.java
 <br>	    se crea un objeto:
<br>	    
<br>				- **Tipos :** showTypesOfShoepartGridview(familyIndex,...) -> apiConnect(url...) -> ...
<br>				
<br>				- **Subpars :** showShoeSubpartsHorizontalScroll(familyIndex,...) -> apiConnect(url...) -> ...
<br>				
<br>				
<br>				*apiConnect(url...) de **PaletteFragment.java** ejecuta en un hilo la conexi�n a la api:*
<br>				
<br>				1. -> model.apiConnect(url): 
<br>				2. -> model.getSection(0) :*obtiene un ArrayList de bitmaps.*
<br>				3. -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*
<br>				
<br>				
<br>				*a continuaci�n la secuencia de llamadas sigue asi:*
<br>				
<br>				... -> model.apiConnect(url) -> new ApiJMFierroMadeInMe(url...) 
<br>	
 *<br>				Mientras tanto en **PalleteFragment.java** esta a la espera de que la API 
 *<br>				termine las descarga de datos, entonces se manda una mensaje a un **Handler 
 *<br>				mHandler = new Handler()** con un tag que identifica los datos descargados:* 	 
<br>	
<br>	 
<br>	 
<br>		// Materiales
<br>		ModelDummy modelMaterials = new ModelDummy(mContext);
<br>		LinkedHashMap<String, ArrayList<Bitmap>> data = modelMaterials.loadSectionsMaterials(
<br>				aContext,  //getActivity(), 
<br>				shoeFamilyIndex, 
<br>				shoepartIndex, 
<br>				typeOfShoepartIndex,
<br>				subpartIndex);
<br>*
<br> 
<br> 
 *<br>  ------- PaletteFragment.java
 *<br>  |		|
 *<br>  |		|-> palette_design.xml
 *<br>  |		|		|
 *<br>  |		|		|		(include)
 *<br>  |		|		|-------	subparts_shoe_horizontalscroll.xml
 *<br>  |		|		|
 *<br>  |		|		|---- Types de partes de zapatos : Geleria
 *<br>  |		|		|   GridView  ld: gridview_types
 *<br>  |		|		|  
 *<br>  |		|		|---- Subpartes 
 *<br>  |		|		|   <include layout="@layout/subparts_shoe_horizontalscroll"
 *<br>  |		|		|
 *<br>  |		|		------ Materiales
 *<br>  |		|		   <ListView  id:listview
 *<br>  |		|		
 *<br>  |		|		
 *<br>  |		|------	destroyGridviewTypesOfShoepart()
 *<br>  |		|		|   "animacion previa a showGridviewTypesOfShoepart()" 
 *<br>  |		|		
 *<br>  |		|		
 *<br>  |		|------	showGridviewTypesOfShoepart()
 *<br>  |		|		|
 *<br>  |		|		|------- apiConnect(url...)
 *<br>  |		|		|		|
 *<br>  |		|		|		|	(ModelDummy model = new ModelDummy(mContext);)
 *<br>  |		|		|		| -> model.apiConnect(url): 
 *<br>  |		|		|		| -> model.getSection(0) :		*obtiene un ArrayList de bitmaps.*
 *<br>  |		|		|		| -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*
 *<br>  |		|		|
 *<br>  |		|		|------- mListener:
 *<br>  |		|		|		 		gridView.setOnItemClickListener(new OnItemClickListener() {
 *<br>  |		|		|		 			onItemClick(AdapterView<?> parent, View v, int position, long id) {
 *<br>  |		|		|------- id GridView
 *<br>  |		|		|
 *<br>  |		|		-------- GridviewAdapter.java
 *<br>  |		|				 |
 *<br>  |		|				 ----- types_shoepart_gridview_item.xml
 *<br>  |		|		
 *<br>  |		|
 *<br>  |		|
 *<br>  |		|
 *<br>  |		|------	showGalleryShoeSubparts()
 *<br>  |		|		|
 *<br>  |		|		|------- apiConnect(url...)
 *<br>  |		|		|		|
 *<br>  |		|		|		|	(ModelDummy model = new ModelDummy(mContext);)
 *<br>  |		|		|		| -> model.apiConnect(url): 
 *<br>  |		|		|		| -> model.getSection(0) :		*obtiene un ArrayList de bitmaps.*
 *<br>  |		|		|		| -> mHandler.sendMessage(msg): *avisando del fin de la descarga.*
 *<br>  |		|		|
 *<br>  |		|		|------- mListener:
 *<br>  |		|		|		 		setOnClickItemGalleryScrollGalleryListener(
 *<br>  |		|		|		 			new JMFHorizontalScrollView.OnClickItemHorizontalScrollGalleryListener()
 *<br>  |		|		|		 				onClickItem(int position)
 *<br>  |		|		|
 *<br>  |		|		-------- JMFHorizontalScrollView.java
 *<br>  |		|				 |
 *<br>  |		|				 |---- mListener:
 *<br>  |		|				 |		setOnClickItemGalleryListener (OnClickItemGalleryListener listener)
 *<br>  |		|				 |      -> mListener.onItemClick (int position);
 *<br>  |		|				 |
 *<br>  |		|				 |---- idItemLayout Linearlayout
 *<br>  |		|				 |
 *<br>  |		|				 |----	public int mIdHorizontalScroll = R.id.gellery_horizontalScroll;
 *<br>  |		|				 |		public int mIdGallery = R.id.gallery;
 *<br>  |		|				 |		public int mIdImageContent = R.id.imageContent;
 *<br>  |		|
 *<br>  |		|
 *<br>  |		|
 *<br>  |		|
 *<br>  |		|		
 *<br>  |		-------	showGridviewSectionsMaterials()
 *<br>  |				|
 *<br>  |				|		(LinkedHashMap<String, ArrayList<Bitmap>>)
 *<br>  |				|------- Model().loadSectionsMaterials()
 *<br>  |				|
 *<br>  |				|------- JMFGridviewSections.setupGridviewSections()
 *<br>  |						 |
 *<br>  |						 |---- mListener:
 *<br>  |						 |		setOnGridviewSectionsItemClickListener (OnGridviewSectionsItemClickListener listener) 
 *<br>  |						 |      -> mListener.onItemClick (String sectionName, int position, View v);
 *<br>  |						 |
 *<br>  |						 ---- GridViewSectionsAdapter.java
 *<br>  |							 |
 *<br>  |							 |---- public static final int CHILD_SPACING = 3;
 *<br>  |							 |---- materials_gridviewsections_section_header-xml
 *<br>  |							 |---- materials_gridviewsections_list_row.xml (lisview)
 *<br>  |							 |---- gridviewsections_row_item.xml (item gridview)
 *<br> 
 *<br> 
<br> */
public class PaletteFragment2 extends Fragment {
	//	@TargetApi(Build.VERSION_CODES.GINGERBREAD) public class PaletteFragment extends Fragment {


	/* ------------------------
	 * Listener para item click
	   ------------------------*/
	private OnClickListener mListener;
	public interface OnClickListener {
		public void onSetModel(ModelDummy model);
		public void onClickItemMaterials(int shoeparIndex, int typeIndex, int subpartsIndex, int materialsSectionIndex, int materialsIndex);
	}
	public void setOnClickListener(OnClickListener listener) {
		this.mListener = listener;
	}


	/*
	 * Atributos
	 */

	//	public static final String EXTRA_SHOEPART = "EXTRA_SHOEPART";
	public static final String PREF_TYPESELECTED_BACK = "pref_typeSelected_back";

	private GridView mGridviewTypes;
	private View mViewMaterials;
	private HashMap<Integer, Bitmap> mTypeSelected;

	private int mShoepartCurrent; 
	private int mTypePositionCurrent[];
	private int mSubpartsPositionCurrent[];
	private HashMap<String, Integer> mMaterialsSectionCurrent;
	private HashMap<String, Integer> mMaterialsPositionCurrent;

	private View mViewPalette;
	private View mViewSubparts;
//	private GridView mGridViewSubparts;

	private Context mContext;
	private int mShoeFamilyCurrent;


	/** ............................................................
	 * <br> Recibe el aviso cuando un hilo de descarga(api) ha terminado.
	  ..............................................................*/
	private Handler mHandler = new Handler(){


		@Override
		public void handleMessage(Message msg) {

			View gridview;
			ArrayList<Bitmap> dataArrayList;
			View progress;
			switch(msg.what){

			/* 
			 * Aviso de que la api de descarga para 'Types' esta completado
			 */
			case ModelDummy.DOWNLOAD_TYPES_COMPLETED:

				gridview = (View) mViewPalette.findViewById(R.id.gridview_types);
				gridview.setVisibility(View.VISIBLE);

				
				/*
				 * Datos
				 */
				dataArrayList = (ArrayList<Bitmap>) msg.obj;
				dataArrayList = new ModelDummy(mContext).loadTypesOfShoepart(getActivity(), mShoeFamilyCurrent, mShoepartCurrent);
				
 
				//        		showTypesOfShoepartGridview (dataArrayList, mShoeFamilyCurrent, mShoepartCurrent);
				//				mGridviewTypes = showGridview(mViewPalette, R.id.gridview_types, dataArrayList, mTypePositionCurrent, mShoepartCurrent);
				mGridviewTypes = showGridview(mViewPalette, R.id.gridview_types, dataArrayList, mShoepartCurrent, mTypePositionCurrent, getTypePositionCurrent());
				
				/* ---------
				 * Subpartes
			       ---------*/
				showShoeSubpartsHorizontalScroll(mViewPalette, mShoeFamilyCurrent, mShoepartCurrent, 
						getTypePositionCurrent(), getSubpartPositionCurrent());


				/* ----------------
				 *   Listener item
        		   ----------------*/
				//		mTypesGridView[aShoepart].setOnItemClickListener(new OnItemClickListener() {
				mGridviewTypes.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							int position, long id) {

						
						setGridviewPosition(mGridviewTypes, mTypePositionCurrent, mShoepartCurrent , position);

						
						/* --------
						 * GridView
						   --------*/
//						GridviewAdapter adapter = (GridviewAdapter)mGridviewTypes.getAdapter();
//						adapter.notifyDataSetChanged();
						
//						if (Utils.isTablet(mContext))
//							updateTypeSelected(v?);

						/* ---------
						 * Subpartes
        			       ---------*/
						showShoeSubpartsHorizontalScroll(mViewPalette, mShoeFamilyCurrent, mShoepartCurrent, 
								getTypePositionCurrent(), getSubpartPositionCurrent());

					}
				});

				progress = (View) mViewPalette.findViewById(R.id.gridview_progressBar_types);
				progress.setVisibility(View.GONE);

				break;


				/* 
				 * Aviso de que la api de descarga para 'Subparts' esta completado
				 */
			case ModelDummy.DOWNLOAD_SUBPARTS_COMPLETED:

				gridview = (View) mViewPalette.findViewById(R.id.gridview_types);
				gridview.setVisibility(View.VISIBLE);

				
				/* -----
				 * Datos
				   -----*/
				dataArrayList = (ArrayList<Bitmap>) msg.obj;
				dataArrayList = new ModelDummy(mContext).loadSubpartsOfType(getActivity(), mShoeFamilyCurrent, mShoepartCurrent, getTypePositionCurrent());

				
				
				/* --------
				 * GridView
				   --------*/
				//            	showTypesOfShoepartGridview (dataArrayList, mShoeFamilyCurrent, mShoepartCurrent);
				showShoeSubpartsHorizontalScroll(dataArrayList, mViewPalette, mShoeFamilyCurrent, mShoepartCurrent, 
						getTypePositionCurrent(), getSubpartPositionCurrent());
				mViewSubparts = showGridview(mViewPalette, R.id.gridview_subparts, dataArrayList, mShoepartCurrent, mSubpartsPositionCurrent,  getSubpartPositionCurrent());



				/* ----------------
				 *   Listener item
        		   ----------------*/
				//		mTypesGridView[aShoepart].setOnItemClickListener(new OnItemClickListener() {
				((GridView)mViewSubparts).setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							int position, long id) {


						setGridviewPosition(mViewSubparts, mSubpartsPositionCurrent, mShoepartCurrent , position);

						
						/* ---------
						 * GridView
						   ---------*/
						GridviewAdapter adapter = (GridviewAdapter)((GridView) mViewSubparts).getAdapter();
						adapter.notifyDataSetChanged();

						
//						/*
//						 *  Subpartes
//						 */
//						//						mSubpartsPositionCurrent[mShoepartCurrent] = subpartPosition;
//						//						setSubpartPositionCurrent(subpartPosition);
//						setSubpartPositionCurrent(getSubpartPositionCurrent());



						/* ----------
						 * Materiales
						   ---------*/
						// Actualiza vista materiales
						showMaterialsGridviewSections(getActivity()); 
						//						showMaterialsGridviewSections(
						//								getActivity(), 
						//								mViewPalette,
						//								aShoeFamilyIndex,
						//								aShoepartIndex, 
						//								aTypeOfShoepartIndex, 
						//								subpartPosition);

					}
				});


				progress = (View) mViewPalette.findViewById(R.id.horizontalscroll_progressBar_subparts);
				progress.setVisibility(View.GONE);

				break;
			}
		}
	};


	public static final PaletteFragment2 newInstance(int shoeFamilyIndex, int shoePartIndex){

		PaletteFragment2 f = new PaletteFragment2();

		// Supply num input as an argument.
		Bundle args = new Bundle();
		args.putInt(DesignActivity.ARG_FAMILY, shoeFamilyIndex);
		args.putInt(DesignActivity.ARG_PART, shoePartIndex);
		f.setArguments(args);

		return f;
	}


	/**
	 * NO OCULTAR la visibilidad del constructor vac�o public 
	 * ya que de hacerlo, nuestra aplicaci�n provocar� un fallo 
	 * en ciertas ocasiones en las que debe recrear el Fragment.    
	 */
	public PaletteFragment2(){
	}


	//El fragment se ha adjuntado al Activity
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		mContext = activity;
	}

	//El Fragment ha sido creado        
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Shoeparts
		mShoeFamilyCurrent = getArguments() != null ? getArguments().getInt(DesignActivity.ARG_FAMILY) : ModelDummy.SHOEPART_BODY;
		mShoepartCurrent = getArguments() != null ? getArguments().getInt(DesignActivity.ARG_PART) : ModelDummy.SHOEPART_BODY;

		// Types
		mTypePositionCurrent = new int[ModelDummy.SHOEPARTS.length];
		mTypeSelected = new HashMap<Integer, Bitmap>();

		// Subparts
		mSubpartsPositionCurrent = new int[100];

		// Materials
		mMaterialsSectionCurrent = new HashMap<String, Integer>();
		mMaterialsPositionCurrent = new HashMap<String, Integer>();

	}

	//El Fragment va a cargar su layout, el cual debemos especificar
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (Utils.isTablet(mContext))
			mViewPalette = inflater.inflate(R.layout.palette_design_v7, container, false);
		else
			mViewPalette = inflater.inflate(R.layout.palette_design_v9movil, container, false);

		updateTitle(mShoepartCurrent);
		showTypesOfShoepartGridview(mShoeFamilyCurrent, mShoepartCurrent);

		return mViewPalette;

	}

	//La vista de layout ha sido creada y ya est� disponible
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);


		/* ----------------------------------------------------------
		 * Inicializa el 'view' para  mostrar el 'tipo' seleccionado.
		 *  � S�lo tablet !
		   ----------------------------------------------------------*/
		if (Utils.isTablet(mContext)) {
			View bubbleSelected = (View) view.findViewById(R.id.imageContentSelected_layout);

			// restaura fondo guardado.
			SharedPreferences prefer = PreferenceManager.getDefaultSharedPreferences(getActivity());
			int indexBack = prefer.getInt(PREF_TYPESELECTED_BACK, 1);
			setBubbleTypeSelectedSkin(indexBack);

			/*
			 * Listener 1 para el 'tipo' seleccionado.
			 */
			bubbleSelected.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					/* ------------------------------------------------------------------
					 * Animaci�n para indicar que hay quer  pulsar sobre 'shoeSubparts'.
				   -----------------------------------------------------------------*/
					Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
					mViewPalette.findViewById(R.id.mySubpartsGallery_layout).startAnimation(anim);
				}
			});


			// Listener 2
			bubbleSelected.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					/* --------------------------------------------------------------
					 * Va cambiando al siguiente fondo de la 'burbuja typeSelected'.
				   --------------------------------------------------------------*/
					SharedPreferences prefer = PreferenceManager.getDefaultSharedPreferences(getActivity());
					int indexBack = prefer.getInt(PREF_TYPESELECTED_BACK, 1);

					int newIndex = indexBack+1>3 ? 1 : indexBack+1;
					setBubbleTypeSelectedSkin(newIndex);

					Editor edit = prefer.edit();
					edit.putInt(PREF_TYPESELECTED_BACK, newIndex);
					edit.commit();
					//				edit.apply();

					return false;
				}
			});
		}
	}



	//La vista ha sido creada y cualquier configuraci�n guardada est� cargada
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		//		refreshFragmentTypeOfShoepart();
	}


	//El Activity que contiene el Fragment ha terminado su creaci�n
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	//El Fragment ha sido quitado de su Activity y ya no est� disponible
	@Override
	public void onDetach() {
		super.onDetach();
	}



	/**
	public void refreshFragmentTypeOfShoepart (int shoepart) {

		FragmentManager frgManager = getActivity().getSupportFragmentManager();
		FragmentTransaction frgTrans = frgManager.beginTransaction();

		if (shoepart == 0) {
			Bundle bundle = new Bundle();
			bundle.putInt(EXTRA_SHOEPART, 1); 
			TypesGridviewFragment frgDesignTypesOfShoepart = new TypesGridviewFragment().newInstance(shoepart);

			//		frgTrans.replace(R.id.typesOfShoepart_fragment, frgDesignTypesOfShoepart,MainDesignActivity.TAG_TYPES_OF_SHOEPART);
			frgTrans.add(R.id.typesOfShoepart_fragment, frgDesignTypesOfShoepart);
			frgTrans.commit();

		}

		//		else if (shoepart == 1) {
		//			Bundle bundle1 = new Bundle();
		//			bundle1.putInt(EXTRA_SHOEPART, 2); 
		//			GridviewFragment1 frgDesignTypesOfShoepart1 = new GridviewFragment1().newInstance(bundle1);
		//
		//			FragmentTransaction frgTrans1 = frgManager.beginTransaction();
		//			frgTrans1.add(R.id.typesOfShoepart_fragment1, frgDesignTypesOfShoepart1);
		//			frgTrans1.commit(); 
		//		
		//		}

		//		bundle.putInt(EXTRA_MESSAGE, 2);
		//		GridviewFragment frgDesignTypesOfShoepart2 = new GridviewFragment().newInstance(bundle);
		//
		////		frgTrans.replace(R.id.typesOfShoepart_fragment, frgDesignTypesOfShoepart,MainDesignActivity.TAG_TYPES_OF_SHOEPART);
		//		frgTrans.add(R.id.typesOfShoepart_fragment2, frgDesignTypesOfShoepart1);

		//		frgTrans.commit(); 
	}

	public void refreshFragmentSubpartsOfType () {

		FragmentManager frgManager2 = getActivity().getSupportFragmentManager();
		FragmentTransaction frgTrans2 = frgManager2.beginTransaction();
		//		PartsOfTypeGalleryFragment frgDesignPartsOfTypes = new PartsOfTypeGalleryFragment().newInstance(null);
		JMFHorizontalScrollGalleryFragment frgDesignSubpartsOfTypes = new JMFHorizontalScrollGalleryFragment().newInstance(null);
		frgTrans2.replace(R.id.shoeSubpartsOfTypes_fragment, frgDesignSubpartsOfTypes, DesignActivity.TAG_SUBPARTS_OF_TYPE);
		frgTrans2.commit(); 
	}

	public void refreshFragmentMaterials () {

		FragmentManager frgManager = getActivity().getSupportFragmentManager();
		FragmentTransaction frgTrans = frgManager.beginTransaction();

		GridviewWithSectionsFragment frgDesignMaterials = new GridviewWithSectionsFragment().newInstance(null);
		frgTrans.add(R.id.materials_fragment, frgDesignMaterials,DesignActivity.TAG_MATERIALS);
		frgTrans.commit();	
	}
	 */

	/** ....................................
	 * <br> 
	 * <br> Finaliza el view de typesOfShoeparts: 
	 * <br> realiza una animaci�n y al t�rminar esta 
	 * <br> llama a showTypesOfShoepartGridview(shoepart);
	 * <br> 
	 .......................................*/
	public void destroyAndShowGridviewTypesOfShoepart (final int shoeFamilyIndex, final int shoePartIndex) {

		if (shoePartIndex != mShoepartCurrent ) {

			mShoepartCurrent = shoePartIndex;

			showTypesOfShoepartGridview(shoeFamilyIndex, shoePartIndex);


			/* ---------
			 * Animacion
			 /*---------*/
			/**
			 * Me da problemas tras llamar a api y devuelve con datos vacios.
			 * 
			GridView gridView = (GridView) mViewPalette.findViewById(R.id.gridview_types);
			gridView.setVisibility(View.VISIBLE);
			Animation anim2 = AnimationUtils.loadAnimation(getActivity(), R.anim.translate_overshoot_down);

			anim2.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation arg0) {
					if (mShoeSubpartsView!=null) mShoeSubpartsView.setVisibility(View.INVISIBLE);
				}
				@Override
				public void onAnimationRepeat(Animation arg0) {}
				@Override
				public void onAnimationEnd(Animation arg0) {
					showTypesOfShoepartGridview(shoeFamilyIndex, shoePartIndex);
				}
			});

			gridView.startAnimation(anim2);
			 */

		}
	}




	/** .........................................
	 * <br> 
	 * <br> Muestra los tipos de una parte del zapato 
	 * <br> en un gridview de una columna. 
	 * <br> 
	  ...........................................*/
	public void showTypesOfShoepartGridview (final int aShoeFamilyIndex, final int aShoePartIndex) {

		mShoepartCurrent = aShoePartIndex;

		/*
		 * Datos
		 */
		int tagHandleCompleted = ModelDummy.DOWNLOAD_TYPES_COMPLETED;
		int idView = R.id.gridview_types;
		int idProgress = R.id.gridview_progressBar_types;

		if (aShoePartIndex == ModelDummy.SHOEPART_BODY)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_body/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_TOECAP)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_toecap/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_BACK)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_back/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_HEEL)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_heel/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_ORNAMENT_FRONT)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_ornamentfront/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_ORNAMENT_BACK)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_ornamentback/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_ORNAMENT_TAPE)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_ornamenttape/",idView, idProgress, tagHandleCompleted);
		else if (aShoePartIndex == ModelDummy.SHOEPART_EXTRAS)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/types_extras/",idView, idProgress, tagHandleCompleted);

	}

	/** ..............
	 * <br> 
	 * <br> Gridview 
	  ................*/
	public GridView showGridview (View parent, int idGridView, ArrayList<Bitmap> dataArrayList, int aCategoryIndex, int aPositionArray[], final int aPosition) {
		//		public void showTypesOfShoepartGridview (ArrayList<Bitmap> dataArrayList, final int aShoeFamilyIndex, final int aShoePartIndex) {


		/*
		 * Gridview
		 */
		final GridView gridView;
		gridView = (GridView) parent.findViewById(idGridView);
		//		gridView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
		GridviewAdapter customGridAdapter = new GridviewAdapter(
				getActivity(), 
				R.layout.gridview_item, 
				dataArrayList);
		gridView.setAdapter(customGridAdapter);

		/*
		 * 
		 */
//		gridView.setTag(R.id.id_positions_currentsarray, mTypePositionCurrent);
		gridView.setTag(R.id.id_positions_array, aPositionArray);
		gridView.setTag(R.id.id_positions_array_Index, aCategoryIndex);




		/* ---------------------------------
		 *  Animaci�n y Muestra 'subpartes' 
		   ---------------------------------*/
		//		Utils.animationDownView(mTypesGridView[aShoepart]);
		//		Animation anim2 = AnimationUtils.loadAnimation(getActivity(), R.anim.translate_overshoot_down);
		AnimationSet set = new AnimationSet(true);

		Animation anim = new AlphaAnimation(0.0f, 1.0f);
		//		animation.setDuration(100);
		//		set.addAnimation(animation);

		anim = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
				);
		anim.setDuration(200);
		set.addAnimation(anim);

		anim.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation arg0) {
				//				if (mShoeSubpartsView!=null) mShoeSubpartsView.setVisibility(View.INVISIBLE);
			}
			@Override
			public void onAnimationRepeat(Animation arg0) {}
			@Override

			public void onAnimationEnd(Animation arg0) {

				/** -------------------------------------------
				 * S�lo cuando t�rmina la animaci�n hace scroll 
				 * para que el item actual sea visible.
				   --------------------------------------------*/
//				int shoeTypeCurrent = mTypePositionCurrent[mShoepartCurrent];
//				gridView.smoothScrollToPosition(shoeTypeCurrent);
				gridView.smoothScrollToPosition(aPosition);



				/** ---------------
				 * 
				 * ** Subpartes **
				 * 
				   -----------------*/
				//				int subpartCurrent = mSubpartsPositionCurrent[aPosition];
				//				showShoeSubpartsHorizontalScroll(mViewPalette, aShoeFamilyIndex, aPosition, shoeTypeCurrent, getSubpartPositionCurrent());
			}
		});

		gridView.startAnimation(anim);



		/* ---------------------------
		 * Guarda posicion item 'tipo.
		  ----------------------------*/
//		int positionCurrent = (Integer) mTypePositionCurrent[aPosition];
//		//		View v = Utils.getItemView(gridView);
//		//		View v = (View) mGridviewTypes.getTag(R.id.id_item_view);
//		//		setTypesOfShoepartToTag(aPosition, v, positionCurrent);


//		/* ---------------------------
//		 * Actualiza 'Type' selected.
//		  ----------------------------*/
//		View v = Utils.getItemView(gridView);
//		if (Utils.isTablet(mContext)) {
//
//			ImageView imageView = (ImageView) v.findViewById(R.id.imageContent);
//			updateTypeSelected(imageView.getDrawable());
//			//			Bitmap typeBitmap = mTypeSelected.get(mShoepartCurrent);
//			//			updateTypeSelected(typeBitmap);
//		}

		return gridView;

	}







	//    /** ....................................................................................................
	//     * <br> 
	//     * <br> Muestra los tipos de una parte del zapato 
	//     * <br> en un gridview de una columna. 
	//     * <br> 
	//     * <br> Utiliza 'GridviewAdapter.java', hace una animaci�n, 
	//     * <br> realiza scroll si es necesario y llama a 
	//     * <br> showShoeSubpartsHorizontalScroll(mViewPalette, mShoepartCurrent, shoeTypeCurrent, subpartCurrent); 
	//     * <br> para mostrar el view de las 'subpartes' correspondiente al 'tipo' actual guardado.
	//     * <br> 
	//	  ......................................................................................................*/
	//	public void showTypesOfShoepartGridview (ArrayList<Bitmap> dataArrayList, final int aShoeFamilyIndex, final int aShoePartIndex) {
	//
	//		mShoepartCurrent = aShoePartIndex;
	//
	//		/*
	//		 * Gridview
	//		 */
	//		mGridviewTypes = (GridView) mViewPalette.findViewById(R.id.gridview_types);
	//		//		gridView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
	//		GridviewAdapter customGridAdapter = new GridviewAdapter(
	//				getActivity(), 
	//				R.layout.types_shoepart_gridview_item, 
	//				dataArrayList);
	//		mGridviewTypes.setAdapter(customGridAdapter);
	//
	//		/*
	//		 * 
	//		 */
	//		mGridviewTypes.setTag(R.id.id_positions_currentsarray, mTypePositionCurrent);
	//		mGridviewTypes.setTag(R.id.id_categoty, aShoePartIndex);
	//
	//
	//
	//
	//		/* ---------------------------------
	//		 *  Animaci�n y Muestra 'subpartes' 
	//		   ---------------------------------*/
	//		//		Utils.animationDownView(mTypesGridView[aShoepart]);
	//		//		Animation anim2 = AnimationUtils.loadAnimation(getActivity(), R.anim.translate_overshoot_down);
	//		AnimationSet set = new AnimationSet(true);
	//
	//		Animation anim = new AlphaAnimation(0.0f, 1.0f);
	//		//		animation.setDuration(100);
	//		//		set.addAnimation(animation);
	//
	//		anim = new TranslateAnimation(
	//				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	//				Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
	//				);
	//		anim.setDuration(200);
	//		set.addAnimation(anim);
	//
	//		anim.setAnimationListener(new Animation.AnimationListener() {
	//			@Override
	//			public void onAnimationStart(Animation arg0) {
	//				if (mShoeSubpartsView!=null) mShoeSubpartsView.setVisibility(View.INVISIBLE);
	//			}
	//			@Override
	//			public void onAnimationRepeat(Animation arg0) {}
	//			@Override
	//
	//			public void onAnimationEnd(Animation arg0) {
	//
	//				/** -------------------------------------------
	//				 * S�lo cuando t�rmina la animaci�n hace scroll 
	//				 * para que el item actual sea visible.
	//				   --------------------------------------------*/
	//				int shoeTypeCurrent = mTypePositionCurrent[mShoepartCurrent];
	//				mGridviewTypes.smoothScrollToPosition(shoeTypeCurrent);
	//
	////				Bitmap bitmapType = (Bitmap)mGridviewTypes.getAdapter().getItem(mShoepartCurrent);
	////				updateTypeSelected(bitmapType);
	//
	//
	//				/** ---------------
	//				 * 
	//				 * ** Subpartes **
	//				 * 
	//				   -----------------*/
	//				int subpartCurrent = mSubpartsPositionCurrent[aShoePartIndex];
	//				showShoeSubpartsHorizontalScroll(mViewPalette, aShoeFamilyIndex, aShoePartIndex, shoeTypeCurrent, getSubpartPositionCurrent());
	////				int subpartCurrent = mSubpartsPositionCurrent[mShoepartCurrent];
	////				showShoeSubpartsHorizontalScroll(mViewPalette, mShoepartCurrent, shoeTypeCurrent, getSubpartPositionCurrent());
	//			}
	//		});
	//
	//		mGridviewTypes.startAnimation(anim);
	//
	//
	//
	//		/* ---------------------------
	//		 * Guarda posicion item 'tipo.
	//		  ----------------------------*/
	//		int position = (Integer) mTypePositionCurrent[aShoePartIndex];
	//		View v = Utils.getItemView(mGridviewTypes);
	//		//		View v = (View) mGridviewTypes.getTag(R.id.id_item_view);
	//		setTypesOfShoepartToTag(aShoePartIndex, v, position); 
	//
	//		
	//		/* --------------------------
	//		 *  Check item 'tipo' current
	//		   --------------------------*/
	////		checkTypeOfShoepartItem(v);
	//
	//		
	//
	//		/* ----------------
	//		 *   Listener item
	//		   ----------------*/
	//		//		mTypesGridView[aShoepart].setOnItemClickListener(new OnItemClickListener() {
	//		mGridviewTypes.setOnItemClickListener(new OnItemClickListener() {
	//			@Override
	//			public void onItemClick(AdapterView<?> parent, View v,
	//					int position, long id) {
	//
	//				setTypesOfShoepartToTag(aShoePartIndex, v, position);
	//				
	//				GridviewAdapter adapter = (GridviewAdapter)mGridviewTypes.getAdapter();
	//				adapter.notifyDataSetChanged();
	//
	//				/* ---------
	//				 * Subpartes
	//			       ---------*/
	//				showShoeSubpartsHorizontalScroll(mViewPalette, mShoeFamilyCurrent, mShoepartCurrent, 
	//						getTypePositionCurrent(), getSubpartPositionCurrent());
	//
	//			}
	//		});
	//
	//	}
	//


	private void checkTypeOfShoepartItem(View v) {

		if (v!=null)
			v.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.back_radius10_blacktrans5));

	}






	/** ..........................................................................................
	 * <br> 
	 * <br>  Guarda posici�n, muestra item seleccionado y la vista de subpartes.
	 * <br> 
	 * <br> @param aShoepart:	-> tipo zapato
	 * <br> @param v:			-> item actual (ImageView)
	 * <br> @param typePosition		-> posicion (para identificarse en la llamada a showShoeSubparts())
	 * <br> 
 ..............................................................................................*/
	//	private void setTypesOfShoepartPositionCurrent (View parent, int aShoepart, int position) {
	//
	//		View v = Utils.checkItem(parent, aShoepart, position);
	//		setTypesOfShoepartPositionCurrent(aShoepart, v, position);
	//	}
	//	private void setTypesOfShoepartToTag (int shoepartIndex, View v, int typePosition) {
	//
	//		if (v !=null) {
	//
	//
	//			/* ----------------------
	//			 * Guarda posici�n actual
	//			   ----------------------*/
	//			mTypePositionCurrent[shoepartIndex] = typePosition;
	//			mGridviewTypes.setTag(R.id.id_positions_currentsarray, mTypePositionCurrent);
	//			//			mGridviewTypes.setTag(R.id.id_item_view,v);
	//
	//
	//
	//			/* -------------------------------------
	//			 *  Muesta arriba la image seleccionada
	//			  --------------------------------------*/
	//			if (Utils.isTablet(mContext)) {
	//
	//				ImageView imageView = (ImageView) v.findViewById(R.id.imageContent);
	//				updateTypeSelected(imageView.getDrawable());
	//				//			Bitmap typeBitmap = mTypeSelected.get(mShoepartCurrent);
	//				//			updateTypeSelected(typeBitmap);
	//			}
	//
	//			else {
	//				Bitmap typeBitmapCurrent = mTypeSelected.get(mShoepartCurrent);
	//				updateTypeSelected(typeBitmapCurrent);
	//
	//			}
	//
	//		}
	//
	//	}

	/** ..........................
	 *<br> 
	 *<br>  Guarda posici�n actual
	 *<br>  
	  ............................*/
	private void setGridviewPosition (View view, int positionArray[], int index, int position) {

		positionArray[index] = position;
		if (view != null)
			view.setTag(R.id.id_positions_array, positionArray);
		//			mGridviewTypes.setTag(R.id.id_item_view,v);

	}


	
	
	/** ......................................
	 * <br> 
	 * <br> Actualiza item seleccionado en 'Types'
	 * <br> 
	 ..........................................*/
	private void updateTypeSelected (Drawable dw) {

		updateTypeSelected(Utils.drawableToBitmap(dw));

	}


	
	/** ......................................
	 * <br> 
	 * <br> Actualiza item seleccionado en 'Types'
	 * <br> 
	 ..........................................*/
	private void updateTypeSelected (View gridView) {

		View v = Utils.getItemView(gridView);
		if (v !=null) {

			ImageView imageView = (ImageView) v.findViewById(R.id.imageContent);
			updateTypeSelected(imageView.getDrawable());
		}
	}
	
	
	
	/** ......................................
	 * <br> 
	 * <br> Actualiza item seleccionado en 'Types'
	 * <br> 
	 ..........................................*/
	private void updateTypeSelected (int shoepartIndex, View v, int typePosition) {

		Bitmap typeBitmap = mTypeSelected.get(mShoepartCurrent);
		updateTypeSelected(typeBitmap);

	}
	

	
	/** ......................................
	 * <br> 
	 * <br> Actualiza item seleccionado en 'Types'
	 * <br> 
	 ..........................................*/
	private void updateTypeSelected (Bitmap bitmap) {

		View imageSelectedLayout = (View) mViewPalette.findViewById(R.id.imageContentSelected_layout);
		ImageView imageSelected = (ImageView) mViewPalette.findViewById(R.id.imageContentSelected);

		if (bitmap != null) {
			// Set
			imageSelectedLayout.setVisibility(View.VISIBLE);
			imageSelected.setImageBitmap(bitmap);

			// Animaci�n
			//		Utils.animationDownView(imageSelected);

			mTypeSelected.put(mShoepartCurrent, bitmap);
		}
		else
			imageSelectedLayout.setVisibility(View.INVISIBLE);

	}


	/** ..............................................................
	 * <br> 
	 * <br> Actualiza en la vista el titulo de la parte del zapato actual.
	 * <br> 
	 ................................................................*/
	public void updateTitle(int shoepartIndex) {

		TextView shoepartTitle = (TextView) mViewPalette.findViewById(R.id.shoepartTitleTextView);
		shoepartTitle.setVisibility(View.VISIBLE);

		String shoeparnNames[] = getActivity().getResources().getStringArray(R.array.ShoepartName); 
		shoepartTitle.setText(shoeparnNames[shoepartIndex]);

		Utils.animationDownView(shoepartTitle);

	}



	//	/** ...........................
	//	 * <br> 
	//	 * <br> Quita la vista de subpartes.
	//	 * <br> 
	//	   ............................*/
	//	private void destroyShoesubpartsHorizontalScroll (	
	//			final View aViewPalette, 
	//			final int aFamilyIndex, 
	//			final int aShoepartIndex, 
	//			final int aTypeOfShoepartIndex) {
	//
	//
	//		/*
	//		 * Datos
	//		 */
	//		ArrayList<Bitmap> data = new ArrayList<Bitmap>();
	//		data = new ModelDummy(mContext).loadSubpartsOfType(getActivity(), aShoepartIndex, aTypeOfShoepartIndex);
	//
	//		/* ----------------------------------------
	//		 * Visualiza shoeSubparts con una animaci�n
	//		/* ---------------------------------------- */
	//		mShoeSubpartsView = (View) aViewPalette.findViewById(R.id.myMaterialsGallery_layout);
	//		Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_fromcenter);
	//		mShoeSubpartsView.startAnimation(anim);
	//		mShoeSubpartsView.setVisibility(View.INVISIBLE);
	//
	//		/*
	//		 * HorizontalScroll: gallery custom
	//		 */
	//		JMFHorizontalScrollGallery horizScroll = new JMFHorizontalScrollGallery(
	//				getActivity(), aViewPalette, 
	//				R.layout.gallery_horizontalscroll_item, 
	//				data, 0);
	//
	//		// Listener para click del item seleccionado.
	//		final View viewPalette = mViewPalette;
	//		horizScroll.setOnClickListener(new JMFHorizontalScrollGallery.OnClickListener() {
	//
	//			@Override
	//			public void onClickItem(int position) {
	//				showMaterialsGridviewSections(getActivity(), viewPalette, aShoepartIndex, aTypeOfShoepartIndex, position);
	//
	//			}
	//		});
	//
	//	}

	/** ..........................................................
	 * <br> 
	 * <br> Muestra las subpartes de un tipo de una parte del zapato
	 * <br> 
	   ...........................................................*/
	private void showShoeSubpartsHorizontalScroll (	
			final View aViewPalette, 
			final int aShoeFamilyIndex, 
			final int aShoePartIndex, 
			final int aTypeOfShoepartIndex, 
			final int aSubpartIndex) {

		setSubpartPositionCurrent(aSubpartIndex);

		/*
		 * <br> Datos
		 */
		int tagHandleCompleted = ModelDummy.DOWNLOAD_SUBPARTS_COMPLETED;
		int idView = R.id.subparts_layout;
		int idProgress = R.id.horizontalscroll_progressBar_subparts;

		if (aTypeOfShoepartIndex%2 == 0)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/subparts0/",0, idProgress, tagHandleCompleted);
		else
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/subparts1/",0, idProgress, tagHandleCompleted);

	}

	private void showShoeSubpartsHorizontalScroll (	
			ArrayList<Bitmap> dataArrayList,
			final View aViewPalette, 
			final int aShoeFamilyIndex, 
			final int aShoepartIndex, 
			final int aTypeOfShoepartIndex, 
			final int aSubpartIndex) {

		//		mSubpartsPositionCurrent[mShoepartCurrent] = aSubpartIndex;
		setSubpartPositionCurrent(aSubpartIndex);

		//			ArrayList<Bitmap> data = new ArrayList<Bitmap>();
		//			data = new ModelDummy(mContext).loadSubpartsOfType(getActivity(), aShoeFamilyIndex, aShoepartIndex, aTypeOfShoepartIndex);

		/* ----------------------------------------
		 * Visualiza 'subpartes' con una animaci�n
		/* ---------------------------------------- */
		mViewSubparts = (View) aViewPalette.findViewById(R.id.mySubpartsGallery_layout);
		Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_fromcenter);
		mViewSubparts.startAnimation(anim);
		mViewSubparts.setVisibility(View.VISIBLE);

		JMFHorizontalScrollGallery horizScroll = new JMFHorizontalScrollGallery(
				getActivity(), aViewPalette, 
				R.layout.gallery_horizontalscroll_item, 
				dataArrayList, aSubpartIndex);

		// Guarda posicion subpartes
		int shoeSubpartCurrent = mSubpartsPositionCurrent[mShoepartCurrent];

		// Listener item 'subpartes'
		final View viewPalette = mViewPalette;
		horizScroll.setOnClickListener(new JMFHorizontalScrollGallery.OnClickListener() {

			@Override
			public void onClickItem(int subpartPosition) {

				/*
				 *  Subpartes
				 */
				//				mSubpartsPositionCurrent[mShoepartCurrent] = subpartPosition;
				setSubpartPositionCurrent(subpartPosition);



				/*
				 * Materiales
				 */
				// Actualiza vista materiales
				showMaterialsGridviewSections(
						getActivity(), 
						mViewPalette,
						aShoeFamilyIndex,
						aShoepartIndex, 
						aTypeOfShoepartIndex, 
						subpartPosition);

			}
		});



		/* --------------------
		 * 
		 * Visualiza materiales
		 *  
		/* -------------------- */
		showMaterialsGridviewSections(
				getActivity(), 
				mViewPalette,
				aShoeFamilyIndex,
				aShoepartIndex, 
				aTypeOfShoepartIndex, 
				shoeSubpartCurrent);

	}





	/** ..................
	 * <br> 
	 * <br>  Muestra materiales
	 * <br>  
	  ....................*/
	public void showMaterialsGridviewSections (Context context) { 

		showMaterialsGridviewSections (
				context,
				mViewPalette,
				mShoeFamilyCurrent,
				mShoepartCurrent, 
				getTypePositionCurrent(), 
				getSubpartPositionCurrent());
	}

	public void showMaterialsGridviewSections (
			Context aContext, 
			View aViewPalette,
			int shoeFamilyIndex, 
			int shoepartIndex, 
			int typeOfShoepartIndex, 
			int subpartIndex) {

		mViewMaterials = (View) mViewPalette.findViewById(R.id.materials_shoes_layout);

		// Datos
		ModelDummy modelMaterials = new ModelDummy(mContext);
		LinkedHashMap<String, ArrayList<Bitmap>> data = modelMaterials.loadSectionsMaterials(
				aContext,  //getActivity(), 
				shoeFamilyIndex, 
				shoepartIndex, 
				typeOfShoepartIndex,
				subpartIndex);

		showMaterialsGridviewSections (
				data,
				aContext, 
				aViewPalette,
				shoeFamilyIndex, 
				shoepartIndex, 
				typeOfShoepartIndex, 
				subpartIndex);


		/*
		 * <br> Datos
		 */
		/**
    	int tagHandleCompleted = ModelDummy.DOWNLOAD_SUBPARTS_COMPLETED;
    	int idView = R.id.subparts_layout;
    	int idProgress = R.id.progressBar_subparts;

		if (aTypeOfShoepartIndex%2 == 0)
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/subparts0/",0, idProgress, tagHandleCompleted);
		else
			apiConnect("http://hidden-reef-8347.herokuapp.com/api/subparts1/",0, idProgress, tagHandleCompleted);
		 */

	}


	public void showMaterialsGridviewSections (
			LinkedHashMap<String, ArrayList<Bitmap>> dataHasdMap,
			Context aContext, 
			View aViewPalette,
			int shoeFamilyIndex, 
			int shoepartIndex, 
			int typeOfShoepartIndex, 
			int subpartIndex) {

		mViewMaterials = (View) mViewPalette.findViewById(R.id.materials_shoes_layout);

		// Datos
		ModelDummy modelMaterials = new ModelDummy(mContext);
		LinkedHashMap<String, ArrayList<Bitmap>> data = modelMaterials.loadSectionsMaterials(
				aContext,  //getActivity(), 
				shoeFamilyIndex, 
				shoepartIndex, 
				typeOfShoepartIndex,
				subpartIndex);

		// listener
		if (mListener != null) 
			mListener.onSetModel(modelMaterials);

		//		LinkedHashMap<String, ArrayList<Bitmap>> data =	new ModelDummy()
		//		.loadSectionsMaterials(
		//				aContext,  //getActivity(), 
		//				shoepartIndex, 
		//				typeOfShoepartIndex,
		//				subpartIndex);

		/*
		 * Gridview
		 */
		//		Utils.applyRotation(mViewMaterials, 0, 45);
		JMFGridviewWithSections materialsGridview = 
				new JMFGridviewWithSections(aContext, aViewPalette, shoepartIndex, typeOfShoepartIndex, 
						shoepartIndex, getMaterialsPositionCurrent(), getMaterialsSectionCurrent(), data);
		//		materialsGridview.setupGridviewSections(aContext, aViewPalette, sectionsData);


		/*
		 * Animaci�n
		 */
		//		Utils.applyRotation(mViewMaterials, 90, 0);
		Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_fromcenter);
		mViewMaterials.startAnimation(anim);


		/* --------------------------------
		 *  Check item 'materiales' current
		   --------------------------------*/
		View v = Utils.getItemView(mViewMaterials);
		Utils.checkItemView (mViewMaterials, v, R.id.imageHighlight);



		/* ---------------------------------
		 *   Listener click item materiales
		   ---------------------------------*/
		materialsGridview.setOnClickListener(new JMFGridviewWithSections.OnClickListener() {

			@Override
			public void onClickItem(String sectionName, int sectionPosition, int position, View v) {

				Utils.checkItemView (mViewMaterials, v, R.id.imageHighlight);
				setMaterialsPositionCurrent(sectionPosition, position);

				/* ------------
				 *  Shoe render
				  -------------*/
				if (mListener != null)
					mListener.onClickItemMaterials(mShoepartCurrent, getTypePositionCurrent(), 
							getSubpartPositionCurrent(), sectionPosition, position);

			}
		});

	}


	//	/** ..........................
	//	 * <br> 
	//	 * <br> Check para item 'materiales
	//	 * <br> 
	//	  .............................*/
	//	private void checkItemMaterials (View v, Drawable drawable) {
	//		
	//		if (v != null) {
	//			ImageView imageView = (ImageView) v.findViewById(R.id.imageContent);
	//			imageView.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_radius10_blacktrans5));
	//		}
	//	}





	/** ......................................................................
	 * <br> 
	 * <br>  Cambia 'skin' de la burbuja donde se muestra el 'Type' seleccionado.
	 * <br> 
	 .........................................................................*/
	private void setBubbleTypeSelectedSkin(int indexBack) {

		View view = (View) mViewPalette.findViewById(R.id.imageContentSelected_layout);

		if (indexBack == 0 || indexBack >= 3) 
			view.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_strokewhite2_radius30topright_blacktrans5));
		else if (indexBack == 1) 
			view.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_strokewhite2_radius30topright_gray_lightgray));
		else if (indexBack == 2) 
			view.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_strokewhite2_radius30topright_gray_white));


	}


	/** =============================================
	 * 
	 * M�todos para devolver las posiciones actuales 
	 * 
	   ==============================================*/


	public int getShoepartCurrent() {

		return mShoepartCurrent;
	}

	public int getTypePositionCurrent() {
		return mTypePositionCurrent[mShoepartCurrent];
	}

	public int getSubpartPositionCurrent() {

		int shoepartPositionCurrent = getShoepartCurrent();

		return mSubpartsPositionCurrent[shoepartPositionCurrent];
	}

	public int setSubpartPositionCurrent(int position) {

		return setSubpartPositionCurrent(getShoepartCurrent(), position); 
	}

	public int setSubpartPositionCurrent(int shoepartIndex, int position) {

		int subpartOld  = mSubpartsPositionCurrent[shoepartIndex];
		mSubpartsPositionCurrent[shoepartIndex] = position;

		return mSubpartsPositionCurrent[shoepartIndex];
	}


	/** ............................................
	 * <br> 
	 * <br> Devuelve la posicion actual de 'Materiales.
	 * <br> 
	   ...........................................*/
	public int getMaterialsPositionCurrent () {
		return getMaterialsPositionCurrent (getShoepartCurrent(), getTypePositionCurrent(), getSubpartPositionCurrent());
	}

	// ...con argumentos
	public int getMaterialsPositionCurrent (int shoepartIndex, int typeOfShoepartIndex, int subpartIndex) {

		String materialsKey = getMaterlasKey(shoepartIndex, typeOfShoepartIndex, subpartIndex);


		int materialPositionCurrent = mMaterialsPositionCurrent.containsKey(materialsKey) 
				? mMaterialsPositionCurrent.get(materialsKey) : -1;

				return materialPositionCurrent;
	}


	/** ............................................
	 * <br> 
	 * <br> Devuelven la secci�n actual de 'Materiales.
	 * <br> 
	   ...........................................*/
	public int getMaterialsSectionCurrent () {

		return getMaterialsSectionCurrent (getShoepartCurrent(), getTypePositionCurrent(), getSubpartPositionCurrent());
	}

	// ...con argumentos
	public int getMaterialsSectionCurrent (int shoepartIndex, int typeOfShoepartIndex, int subpartIndex) {

		String materialsKey = getMaterlasKey(shoepartIndex, typeOfShoepartIndex, subpartIndex);

		int materialSectionCurrent = mMaterialsSectionCurrent.containsKey(materialsKey) 
				? mMaterialsSectionCurrent.get(materialsKey) : -1;

				return materialSectionCurrent;
	}


	/** .....................................................
	 * <br> 
	 * <br> Guarda la posicion y la secci�n actual de 'Materiales.
	 * <br> 
	   ......................................................*/
	public void setMaterialsPositionCurrent (int sectionPosition, int position) {

		setMaterialsPositionCurrent (getShoepartCurrent(), getTypePositionCurrent(), 
				getSubpartPositionCurrent(), sectionPosition, position);
	}


	public void setMaterialsPositionCurrent (int shoepart, int typeOfShoepart, int subpart, int sectionPosition, int position) {

		String materialsKey = getMaterlasKey(shoepart, typeOfShoepart, subpart);

		mMaterialsPositionCurrent.put(materialsKey, position); 
		mMaterialsSectionCurrent.put(materialsKey, sectionPosition); 

	}






	/** ............................................
	 * <br> 
	 * <br> Devuelve la 'clave' para el 'HashMap' de 'Materiales.
	 * <br> 
	   ...........................................*/

	private String getMaterlasKey(int aShoepart, int aTypeOfShoepart, int aSubpart) {

		String materialsKey = 
				String.valueOf(aShoepart)
				+ String.valueOf(aTypeOfShoepart)
				+ String.valueOf(aSubpart);

		return materialsKey;

	}

	/** ..........................................................................................
	 * <br> 
	 * <br>  Guarda en el tag del el 'view' del 'item'
	 * <br> 
 		...........................................................................................*/
	private void setMaterialsPositionsToTag (View parent, View v, int sectionPosition, int position) {

		if (parent !=null) {

			parent.setTag(R.id.id_positions_currents, mMaterialsPositionCurrent);
			parent.setTag(R.id.id_sections_currents, mMaterialsSectionCurrent);
			if (v !=null)
				parent.setTag(R.id.id_item_view,v);

		}
	}



	/** ...........................................
	 * <br> 
	 * <br> Conexi�n a la api para descarga de imagenes. 
	 * <br> Al finalizar la descarga avisa enviando un mensaje a 'mHandler'.
	 * <br> 
	 * <br> @param urlString				-> url de la api.
	 * <br> @param idView				-> view donde se visualizan los datos.
	 * <br> @param idProgressBar			-> progress de espera"
	 * <br> @param tagHandlerCompleted	-> tag para identificarse en 'handle' cunado haya completado la descarga.	
	 * <br> 
	  @see..............................................*/
	public void apiConnect (final String urlString, int idView, int idProgressBar, final int tagHandlerCompleted) {


		/*
		 * Datos
		 */
		//		ArrayList<Bitmap> data = new ModelDummy(mContext).loadTypesOfShoepart(getActivity(), aShoeFamilyIndex, aShoePartIndex);
		// test api
		View view = null;
		if (idView != 0) {
			view = (View) mViewPalette.findViewById(idView);
			view.setVisibility(View.INVISIBLE);
		}

		View progress = (View) mViewPalette.findViewById(idProgressBar);
		progress.setVisibility(View.VISIBLE);

		final ModelDummy model = new ModelDummy(mContext);
		new Thread(new Runnable() {

			@Override
			public void run() {

				boolean isNet = model.apiConnect(urlString);
				ArrayList<Bitmap> dataArrayList = model.getSection(0);

				// Aviso de que la descarga ha fianlizado
				Message msg = Message.obtain();
				msg.what = tagHandlerCompleted;
				msg.obj = dataArrayList;
				msg.arg1 = isNet ? 1 : 0;
				mHandler.sendMessage(msg);

			}
		}).start();


		// Llamada a gridview con los datos
		//		showTypesOfShoepartGridview (dataArrayList, aShoeFamilyIndex, aShoePartIndex);

	}



}





