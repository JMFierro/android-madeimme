package com.utad.madeinme;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class SplashActivity extends Activity {

    MediaPlayer song;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
 
//        song = MediaPlayer.create(Splash.this, R.raw.door); //the song is door.mp3
        song.start();
 
        Thread mythread = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent myIntent = new Intent("com.learning.gilo.MENULIST");
                    startActivity(myIntent);
                }
            }
 
        };
 
        mythread.start();
 
    }
 
    protected void onPause(){
        super.onPause();
        song.release();
        finish();
    }
}