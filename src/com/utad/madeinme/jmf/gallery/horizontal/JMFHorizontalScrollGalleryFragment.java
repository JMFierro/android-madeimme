
package com.utad.madeinme.jmf.gallery.horizontal;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.utad.madeinme.R;
import com.utad.madeinme.jmf.model.ModelDummy;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 * Crea una galeria de view's(ImageView) con scroll horizontal.
 * 
 */
public class JMFHorizontalScrollGalleryFragment extends Fragment {

	private int mCurrentItem;
	private String EXTRA_SHOESUBPART;


	/*------------------------------------
	 * Callback
	 *  Jose Manuel Fierro Conchouso, 2014.
	 --------------------------------------*/
	private OnSetGetData mCallback  = CallbackVoid;

	public interface OnSetGetData {
		public  ArrayList<Bitmap> onGridviewGetData(String tag);
		public void onGridviewItemChanged(int position, String tag);
	}

	private static OnSetGetData CallbackVoid = new OnSetGetData() {
		@Override
		public  ArrayList<Bitmap> onGridviewGetData(String tag) {
			return null;
		}

		@Override
		public void onGridviewItemChanged(int position, String tag) {
		}
	};



//	/* -------------------
//	 * Listener click item
//	   -------------------*/
//	private OnClickItemHorizontalScrollGalleryListener mListener;
//
//	public interface OnClickItemHorizontalScrollGalleryListener{
//		public void onClickItem(int position);
//	}
//
//	public void setOnClickItemGalleryScrollGalleryListener (OnClickItemHorizontalScrollGalleryListener listener) {
//		mListener = listener;
//	}


	public static JMFHorizontalScrollGalleryFragment newInstance(Bundle arguments){
		JMFHorizontalScrollGalleryFragment f = new JMFHorizontalScrollGalleryFragment();
		if(arguments != null){
			f.setArguments(arguments);
		}
		return f;
	}


	/**
	 * NO OCULTAR la visibilidad del constructor vac�o public 
	 * ya que de hacerlo, nuestra aplicaci�n provocar� un fallo 
	 * en ciertas ocasiones en las que debe recrear el Fragment.    
	 */
	public JMFHorizontalScrollGalleryFragment(){
	}


	//El fragment se ha adjuntado al Activity
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback  = CallbackVoid = (OnSetGetData) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement callback's");
		}

	}

	//El Fragment ha sido creado        
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//		mData = getData();
		if (getArguments() != null)
			mCurrentItem = getArguments().getShort(EXTRA_SHOESUBPART);
		else
			mCurrentItem = 0;

	}

	//El Fragment va a cargar su layout.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.gallery_horizontalscroll, container, false);        
	}

	//La vista de layout ha sido creada y ya est� disponible
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	
	/**---------------------------------------------------------------------
	 * 
	 * * Creaci�n gridview y  uso del adaptador
	 * Jose Manuel Fierro Conchouso, 2014. 
     -----------------------------------------------------------------------*/
	//La vista ha sido creada y cualquier configuraci�n guardada est� cargada
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

//		showHorizontalViewScroll(
//				getActivity(),
//				getView(), 
//				R.id.mygallery,
//				getData());

		JMFHorizontalScrollGallery horizScroll = new JMFHorizontalScrollGallery(
				getActivity(),
				getView(), 
				R.id.gallery,
				getData(),
				mCurrentItem);

	}


	//El Activity que contiene el Fragment ha terminado su creaci�n
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	//El Fragment ha sido quitado de su Activity y ya no est� disponible
	@Override
	public void onDetach() {
		super.onDetach();
	}




	/** ------------------
	 * Carga de imagenes
	 --------------------*/
	public ArrayList<Bitmap> getData() {
		return mCallback.onGridviewGetData(getTag());
	}

}


