package com.utad.madeinme.jmf.gridview;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.utad.madeinme.R;
import com.utad.madeinme.utils.Utils;

/** .......................................................................................................<br>
 * 
 * Jose Manuel Fierro Conchouso, 2014.<br>
 * <br>
 * 
 * Adem�s de inflar la vista del item hace una gestion del check (Highlight On/Off).<br>
 * 
	 * @param context
	 * @param layoutResourceId	-> Vista del 'item'
	 * @param data				->  ArrayList<Bitmap> para los 'items'.

 * 
 *   Utiliza varios 'id' ubicados en recursos para acceder los tag() del 'parent' de la vista
 * (Nota JMFierro!: lo ideal ser�a pasar estos datos como par�metros al constructor, lo dejo como tarea pendiente):
 * 
 * 		R.id.id_positions_currentsarray   -> array de las posiciones actuales para cada categor�a.
 *		R.id.id_category				  -> categor�a actual. 
 *
 * 
 * 
 * Fuentes: javatechig {@link http://javatechig.com}
 * 		   	http://javatechig.com/android/android-gridview-example-building-image-gallery-in-android
 * 			http://stackoverflow.com/questions/20658641/setonitemclicklistener-is-not-working-for-arrayadapterimageview
 * 			http://abejaselectricas.wordpress.com/2013/03/25/gridview-en-android-como-crearlo-y-llenarlo-con-datos/
 * 
   ...........................................................................................................*/
public class GridviewAdapter extends ArrayAdapter<Bitmap> {

	private Context mContext;
	private int mLayoutResourceId;
	private ArrayList<Bitmap> mData = new ArrayList<Bitmap>();

	private int mCurrentPosition;

	/** .....................................................................
	 * 
	 * @param context
	 * @param layoutResourceId	-> Vista del 'item'
	 * @param data				->  ArrayList<Bitmap> para los 'items'.
	 * 
	  .......................................................................*/
	public GridviewAdapter(Context context, int layoutResourceId, ArrayList<Bitmap> data) {

		super(context, layoutResourceId, data);

		this.mLayoutResourceId = layoutResourceId;
		this.mContext = context;
		if (data != null)
			this.mData = data;
	}


	/** .....................................................................................................<br>
	 * 
	 *  * Adem�s de inflar la vista del item hace una gestion del check (Highlight On/Off) para los item.<br>
	 * 
	 * 
	 * Utiliza varios 'id' ubicados en recursos para acceder los tag() del 'parent' de la vista<br>
	 * (Nota JMFierro!: lo ideal ser�a pasar estos datos como par�metros al constructor, lo dejo como tarea pendiente):<br>
	 * <br>
	 * 		- R.id.id_positions_currentsarray   -> array de las posiciones actuales para cada categor�a.<br>
	 *		- R.id.id_category				  -> categor�a actual. <br>
	 *
	  .......................................................................................................*/
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		boolean test = false;

		
		/* --------
		 * Inflando
		   --------*/
		View view;
		ImageView imageView;
		if (convertView == null) {
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			view = inflater.inflate(mLayoutResourceId, parent, false);

		} 
		else {
			view = (View) convertView;
		}

		
		/* ----------
		 * Rellenando
		  -----------*/
		imageView = (ImageView) view.findViewById(R.id.imageContent);
		imageView.setImageBitmap(mData.get(position));
		

		/* -----------------------
		 * Highlight On/Off item
		   -----------------------*/
		Utils.checkItemView(mContext, position, view, parent, R.drawable.back_radius10_blacktrans5);
		notifyDataSetChanged();

		return view;	
	}


	
//	@Override
//	public Bitmap getItem(int position) {
//		// TODO Auto-generated method stub
//		return mData.get(position);
//	}
}