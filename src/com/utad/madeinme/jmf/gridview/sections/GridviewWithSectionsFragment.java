
package com.utad.madeinme.jmf.gridview.sections;


import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import com.utad.madeinme.jmf.gridview.GridviewAdapter;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Pr�ctica MadeInMe
 * Master Desarrollo de apps para smartphone y tablet
 * U-Tad
 * 
 * `Gridview' con 'secciones'.
 * 
 * GridView con secciones:
 * 		- El 'Gridview' se crea llamando al m�todo showSectionsGridview() desde onCreateView.
 * 
 * 		- Los datos para rellenar el 'Gridview' se obtienen desde el m�todo getData(), 
 * 		que envia un mensaje al 'callback' onGetData(). Se recive un LinkedHashMap<String, ArrayList<Bitmap>> 
 * 		que se pasar� como parametro al adapter.
 * 
 * 		- B�sicamente es un listview que se rellena con las imagenes que entran en una fila. El n�mero de 
 * 		imagenes que entran en cada fila dependen del tama�o que se les haya dado. (El c�digo ha sido adaptado 
 * 		del ejemplo hecho por 'madhu314' p�blicado en 'github')
 * 
 * onGridviewItemChanged() : callback para avisar de la selecci�n de un item.
 *     
 *     -------
 *  -> Fuentes
 *     -------
 *     
 *     	Estructura:
 *     		- elaboraci�n propia (JMFierro, 2014)
 *  
 *  
 *  	Implementaci�n del fragment:
 *  		- http://javatechig.com/android/android-gridview-example-building-image-gallery-in-android
 *  		- http://jarroba.com/programar-fragments-fragmentos-en-android/
 *  		- http://stackoverflow.com/questions/9889175/is-it-possible-to-refresh-the-view-of-a-fragment
 *  		- http://vikaskanani.wordpress.com/2011/07/20/android-custom-image-gallery-with-checkbox-in-grid-to-select-multiple/
 *  
 *  
 * 		Secciones en gridview:
 *  		- https://github.com/madhu314/sectionedgridview
 *  
 */

public class GridviewWithSectionsFragment extends Fragment {

	/* -----
	 * Datos
	   -----*/
	LinkedHashMap<String, ArrayList<Bitmap>> mSectionsData;
	
	//	public static final String TAG = "GridViewFragment";
	private GridView mGridView;
	private GridviewAdapter mCustomGridAdapter;
	private Context mContex;

	// ++++++++++++++++ Gridview con secciones ++++++++++++++
	private ListView mListView;
	private Dataset mDataSet;
	private GridViewWithSectionsAdapter mSectionsAdapter = null;
	private LinkedHashMap<String, Cursor> mCursorMap;
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++

	
	/** ............................................
	 * 
	 * Callback
	 * 
	 *  @author Jose Manuel Fierro Conchouso, 2014.
	 ................................................*/
	private OnSetListener mCallback  = CallbackVoid;

	public interface OnSetListener {
		public int onGridviewGetLayoutId(String tag);
		public int onGridviewGetId(String tag);
		public LinkedHashMap<String, ArrayList<Bitmap>> onGetSectionsData(String tagIdFragment);
		public void onGridviewItemChanged(int position, String tag);
	}

	private static OnSetListener CallbackVoid = new OnSetListener() {
		@Override
		public LinkedHashMap<String, ArrayList<Bitmap>> onGetSectionsData(String tagIdFragment) {
			return new LinkedHashMap<String, ArrayList<Bitmap>>();
		}

		@Override
		public void onGridviewItemChanged(int position, String tag) {
			// TODO Auto-generated method stub
		}

		@Override
		public int onGridviewGetLayoutId(String tag) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int onGridviewGetId(String tag) {
			// TODO Auto-generated method stub
			return 0;
		}
	};


	/** ........................
	 * 
	 * Listener para item click
	 * 
	   .........................*/
	private OnGridviewSectionsItemClickListener mListener;
	
	public interface OnGridviewSectionsItemClickListener {
		public void onClickItem (int position);
	}
	
	public void setOnGridviewSectionsItemClickListener (OnGridviewSectionsItemClickListener listener) {
		mListener = listener;
	}


	public static GridviewWithSectionsFragment newInstance(Bundle arguments){
		GridviewWithSectionsFragment f = new GridviewWithSectionsFragment();
		if(arguments != null){
			f.setArguments(arguments);
		}
		return f;
	}


	/**
	 * NO OCULTAR la visibilidad del constructor vac�o public 
	 * ya que de hacerlo, nuestra aplicaci�n provocar� un fallo 
	 * en ciertas ocasiones en las que debe recrear el Fragment.    
	 */
	public GridviewWithSectionsFragment(){
	}


	//El fragment se ha adjuntado al Activity
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback  = CallbackVoid = (OnSetListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement onGetData()");
		}
	}

	//El Fragment ha sido creado        
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mSectionsData = getSectionsData();
	}

	//El Fragment va a cargar su layout, el cual debemos especificar
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(mCallback.onGridviewGetLayoutId(getTag()), container, false);        
	}

	//La vista de layout ha sido creada y ya est� disponible
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

//		setupGridviewSections(getActivity(), getView(),getSectionsData());
		new JMFGridviewWithSections().setupGridviewSections(
				getActivity(), getView(),getSectionsData());
	}


	//La vista ha sido creada y cualquier configuraci�n guardada est� cargada
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
	}


	//El Activity que contiene el Fragment ha terminado su creaci�n
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	//El Fragment ha sido quitado de su Activity y ya no est� disponible
	@Override
	public void onDetach() {
		super.onDetach();
	}




	/** ------------------
	 * Crea le grid View
	 --------------------*/
	/**
	private void setupGridview() {
		mGridView = (GridView) getActivity().findViewById(mCallback.onGetGridviewId(getTag()));
		mCustomGridAdapter = new GridViewAdapter(getActivity(), 
				R.layout.gridview_item, 
				this.getData());
		mGridView.setAdapter(mCustomGridAdapter);

		//  Listener item
		mGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				mCallback.onGridviewItemChanged(position, getTag());
				//				Toast.makeText(getActivity(), "" 
				//                        + position, Toast.LENGTH_SHORT).show();
			}
		});
	}
	*/


	/** ------------------
	 * Carga de imagenes
	 --------------------*/
	public LinkedHashMap<String, ArrayList<Bitmap>> getSectionsData() {
		return mCallback.onGetSectionsData(getTag()); // this.mModel.loadTypesOfShoePart(getActivity(), TabsBtnFragment.TYPE_OF_SHOEPART_TOECAP);
	}


}


