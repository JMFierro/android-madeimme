package com.utad.madeinme.jmf.gridview;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.GridView;

import com.utad.madeinme.R;
import com.utad.madeinme.utils.Utils;
//import android.support.v4.app.Fragment;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Visualiza un `Gridview'.
 * 
 */

public class JMFGridview {
//	public class JMFGridview extends Fragment{

	/* --------------------------------------
	 * Listener click item
	   --------------------------------------*/
	private OnItemClickListener mListener;

	public interface OnItemClickListener{
		public void onItemClickJMFGridview(int collectionIndex, int position);
	}

	public void setOnItemClickListener(OnItemClickListener eventListener) {
		mListener=eventListener;
	}


	/* ---------
	 * Atributos
	   ---------*/
	private Context mContext;
//	private boolean mIsAnimation;

	private View mParentView;
	private GridView mGridView;

	// Posiciones actuales: varias colecciones en un gridview.
	private int mCollectionsArray[];
	private  int mCollectionsIndex;

	private int mIdGridView;
	private int mIdGridItemView;
	private ArrayList<Bitmap> mDataBitmapArrayList;
	private GridviewAdapter mGridAdapter;
	//	private GridviewAdapter mCustomGridAdapter;

	private View itemCurrent;

	/** ............................................................
	 * <br> Recibe el aviso cuando un hilo de descarga(api) ha terminado.
	  ..............................................................*/
	//	private Handler mHandler = new Handler(){
	//
	//
	//		@Override
	//		public void handleMessage(Message msg) {
	//
	//			View gridview;
	//			ArrayList<Bitmap> dataArrayList;
	//			View progress;
	//			
	//			switch(msg.what){
	//
	//			/* 
	//			 * Aviso de que la api de descarga para 'Types' esta completado
	//			 */
	//			case ModelDummy.DOWNLOAD_DATA_COMPLETED:
	//				
	//				/*
	//				 * Datos
	//				 */
	////				dataArrayList = (ArrayList<Bitmap>) msg.obj;
	//				mDataBitmapArrayList = (ArrayList<Bitmap>) msg.obj;
	//				
	//////				mGridView = showGridview(mViewPalette, R.id.gridview_types, dataArrayList, mShoepartCurrent, mTypePositionCurrent, getTypePositionCurrent());
	//				mGridView = showGridview(mParentView, mIdGridView, mDataBitmapArrayList, mCategoryCurrent, mPositionArray);
	//
	//				
	//				mListener.onItemClick();
	//				
	//				break;
	//			}
	//		}
	//	};


//	public JMFGridview () {
//
//	}
	
	
//	public static final JMFGridview newInstance(int arg1, int arg2){
//
//		PaletteFragment f = new PaletteFragment();
//
//		// Supply num input as an argument.
//		Bundle args = new Bundle();
//		args.putInt(DesignActivity.ARG_FAMILY, arg1);
//		args.putInt(DesignActivity.ARG_PART, arg2);
//		f.setArguments(args);
//
//		return f;
//	}

	
//	//El fragment se ha adjuntado al Activity
//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//
//		mContext = activity;
//	}


//	//El Fragment ha sido creado        
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//			super.onCreate(savedInstanceState);


//	public JMFGridview(
//			Activity activity,
//			boolean isAnimation,
//			View parentView,
//			int idGridView,
//			int idGridItemView,
//			ArrayList<Bitmap> dataBitmapArrayList,
//			int sizeArrayPositions) {
	public JMFGridview(
			Context context,
			View parentView,
			int idGridView,
			int idGridItemView,
			int sizeArrayPositions) {
		
	
		mContext = context;
//		mContext = activity.getApplicationContext();
//		mListener = (OnItemClickListener) activity;
//		mIsAnimation = isAnimation;

		mParentView = parentView;
		mIdGridView = idGridView;
		mIdGridItemView = idGridItemView;
		
//		mDataBitmapArrayList = dataBitmapArrayList;
		
		mCollectionsArray = new int[sizeArrayPositions];
		
		//		mPosition = position; 

//		mGridView = showGridview();
		mGridView = (GridView) mParentView.findViewById(mIdGridView);

		/*
		 * Listener item GridView
		 */
		mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				
				setPosition(position);
				Utils.checkItemView(mContext, parent, v, R.drawable.back_radius10_blacktrans5);
				
				if (mListener != null)
					mListener.onItemClickJMFGridview(mCollectionsIndex, position);
			}
		});
		

	}


	public Context getContext() {
		return mContext;
	}

	public void setContext(Context context) {
		this.mContext = context;
	}

//	public boolean isAnimation() {
//		return mIsAnimation;
//	}
//
//	public void setIsAnimation(boolean isAnimation) {
//		this.mIsAnimation = isAnimation;
//	}

	public ArrayList<Bitmap> getDataBitmapArrayList() {
		return mDataBitmapArrayList;
	}

	public void setDataBitmapArrayList(ArrayList<Bitmap> dataBitmapArrayList) {
		this.mDataBitmapArrayList = dataBitmapArrayList;
	}

	public int getPositionItem() {
		return mCollectionsArray[mCollectionsIndex];
	}

	public void setPosition(int position) {
		mCollectionsArray[mCollectionsIndex] = position;
	}
	

//	private void listener() {
//		
//		/*
//		 * Listener item GridView
//		 */
//		mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View v,
//					int position, long id) {
//				
//				setPosition(position);				
//			}
//		});
//
//	}
	
	/** ..............
	 * <br> 
	 * <br> Gridview
	 * <br>  
	  ................*/
	public GridView showGridview (ArrayList<Bitmap> dataArrayList, boolean isAnimation, int positionsArrayIndex) {
		//		public void showTypesOfShoepartGridview (ArrayList<Bitmap> dataArrayList, final int aShoeFamilyIndex, final int aShoePartIndex) {

		/*
		 * Set atributos
		 */
		mDataBitmapArrayList  = dataArrayList;
		mCollectionsIndex = positionsArrayIndex;
		Utils.checkItemViewSave(mGridView, mCollectionsArray, positionsArrayIndex);
		

		/*
		 * Gridview
		 */
////		final GridView gridView;
//		mGridView = (GridView) mParentView.findViewById(mIdGridView);
//		//		gridView.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
		mGridAdapter = new GridviewAdapter(
				mContext, 
				mIdGridItemView, 
				mDataBitmapArrayList);

		mGridView.setAdapter(mGridAdapter);

//		/*
//		 * Guardar posiciones en tag(view): uso para Highliht (check) 
//		 */
//		//		gridView.setTag(R.id.id_positions_currentsarray, mTypePositionCurrent);
//		mGridView.setTag(R.id.id_positions_array, mCollectionsArray);
//		mGridView.setTag(R.id.id_positions_array_Index, positionsArrayIndex);


		if (isAnimation)
			animation();

//		/* ---------------------------------
//		 *  Animaci�n y Muestra 'subpartes' 
//		   ---------------------------------*/
//		//		Utils.animationDownView(mTypesGridView[aShoepart]);
//		//		Animation anim2 = AnimationUtils.loadAnimation(getActivity(), R.anim.translate_overshoot_down);
//		AnimationSet set = new AnimationSet(true);
//
//		Animation anim = new AlphaAnimation(0.0f, 1.0f);
//		//		animation.setDuration(100);
//		//		set.addAnimation(animation);
//
//		anim = new TranslateAnimation(
//				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
//				Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
//				);
//		anim.setDuration(200);
//		set.addAnimation(anim);
//
//		anim.setAnimationListener(new Animation.AnimationListener() {
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				//				if (mShoeSubpartsView!=null) mShoeSubpartsView.setVisibility(View.INVISIBLE);
//			}
//			@Override
//			public void onAnimationRepeat(Animation arg0) {}
//			@Override
//
//			public void onAnimationEnd(Animation arg0) {
//
//				/** -------------------------------------------
//				 * S�lo cuando t�rmina la animaci�n hace scroll 
//				 * para que el item actual sea visible.
//				   --------------------------------------------*/
//				//				int shoeTypeCurrent = mTypePositionCurrent[mShoepartCurrent];
//				//				gridView.smoothScrollToPosition(shoeTypeCurrent);
//				gridView.smoothScrollToPosition(getPositionItem());
//
//
//
//				/** ---------------
//				 * 
//				 * ** Subpartes **
//				 * 
//				   -----------------*/
//				//				int subpartCurrent = mSubpartsPositionCurrent[aPosition];
//				//				showShoeSubpartsHorizontalScroll(mViewPalette, aShoeFamilyIndex, aPosition, shoeTypeCurrent, getSubpartPositionCurrent());
//			}
//		});
//
//		gridView.startAnimation(anim);



		/* ---------------------------
		 * Guarda posicion item 'tipo.
		  ----------------------------*/
		//		int positionCurrent = (Integer) mTypePositionCurrent[aPosition];
		//		//		View v = Utils.getItemView(gridView);
		//		//		View v = (View) mGridviewTypes.getTag(R.id.id_item_view);
		//		//		setTypesOfShoepartToTag(aPosition, v, positionCurrent);


		//		/* ---------------------------
		//		 * Actualiza 'Type' selected.
		//		  ----------------------------*/
		//		View v = Utils.getItemView(gridView);
		//		if (Utils.isTablet(mContext)) {
		//
		//			ImageView imageView = (ImageView) v.findViewById(R.id.imageContent);
		//			updateTypeSelected(imageView.getDrawable());
		//			//			Bitmap typeBitmap = mTypeSelected.get(mShoepartCurrent);
		//			//			updateTypeSelected(typeBitmap);
		//		}

		/* --------
		 * Listener
		   --------*/
//		listener();
		
		return mGridView;

	}


	
	
	private void animation () {
		
		/* ---------------------------------
		 *  Animaci�n y Muestra 'subpartes' 
		   ---------------------------------*/
		//		Utils.animationDownView(mTypesGridView[aShoepart]);
		//		Animation anim2 = AnimationUtils.loadAnimation(getActivity(), R.anim.translate_overshoot_down);
		AnimationSet set = new AnimationSet(true);

		Animation anim = new AlphaAnimation(0.0f, 1.0f);
		//		animation.setDuration(100);
		//		set.addAnimation(animation);

		anim = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
				);
		anim.setDuration(200);
		set.addAnimation(anim);

		anim.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation arg0) {
				//				if (mShoeSubpartsView!=null) mShoeSubpartsView.setVisibility(View.INVISIBLE);
			}
			@Override
			public void onAnimationRepeat(Animation arg0) {}
			@Override

			public void onAnimationEnd(Animation arg0) {

				/** -------------------------------------------
				 * S�lo cuando t�rmina la animaci�n hace scroll 
				 * para que el item actual sea visible.
				   --------------------------------------------*/
				//				int shoeTypeCurrent = mTypePositionCurrent[mShoepartCurrent];
				//				gridView.smoothScrollToPosition(shoeTypeCurrent);
				mGridView.smoothScrollToPosition(getPositionItem());



				/** ---------------
				 * 
				 * ** Subpartes **
				 * 
				   -----------------*/
				//				int subpartCurrent = mSubpartsPositionCurrent[aPosition];
				//				showShoeSubpartsHorizontalScroll(mViewPalette, aShoeFamilyIndex, aPosition, shoeTypeCurrent, getSubpartPositionCurrent());
			}
		});

		mGridView.startAnimation(anim);

	}
	
	public void checkItem() {
		
	}

	public void setVisibility(int visibility) {
		
		mGridView.setVisibility(visibility);
	}
	
	public GridView getGridView() {
		
		return mGridView;
	}
}


