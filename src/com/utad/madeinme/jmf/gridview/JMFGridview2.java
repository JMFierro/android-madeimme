package com.utad.madeinme.jmf.gridview;

import java.util.ArrayList;

import com.utad.madeinme.R;
import com.utad.madeinme.fragments.PaletteFragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

/**
 * Jose Manuel Fierro Conchouso, 2014
 *
 * Visualiza un `Gridview'.
 * 
	public void showGridview(
			Context aContext,
			View aView,
			int aIdGridviewLayout,
			int aIdGridviewItem,
			int aShoepart,
			ArrayList<Bitmap> aData) {
 */

public class JMFGridview2 {

	private GridView mGridView;
	private GridviewAdapter mCustomGridAdapter;


	/* --------------------------------------
	 * Listener click item
	 *  Jose Manuel Fierro Conchouso, 2014.
	 	   --------------------------------------*/
	private OnItemClickListener mListener;

	public interface OnItemClickListener{
		public void onItemClick(int position);
	}

	public void setOnItemClickListener(OnItemClickListener eventListener) {
		mListener=eventListener;
	}


	public JMFGridview2() {
	}
	
	public JMFGridview2(
			Context aContext,
			View aView,
			int aIdGridviewLayout,
			int aIdGridviewItem,
			int aShoepart,
			ArrayList<Bitmap> aData) {

		
		showGridview(
				aContext,
				aView,
				aIdGridviewLayout,
				aIdGridviewItem,
				aShoepart,
				aData);
	}


	/** ------------------
	 * Crea le grid View
	 --------------------*/
	public void showGridview(
			Context aContext,
			View aView,
			int aIdGridviewLayout,
			int aIdGridviewItem,
			int aShoepart,
			ArrayList<Bitmap> aData) {

		mGridView = (GridView) aView.findViewById(aIdGridviewLayout);
		mCustomGridAdapter = new GridviewAdapter(
				aContext, 
				aIdGridviewItem, 
				aData);
		mGridView.setAdapter(mCustomGridAdapter);


		//  Listener item
		mGridView.setOnItemClickListener(new GridView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				//				mCallback.onGridviewItemChanged(position, getTag());
				if(mListener != null) mListener.onItemClick(position);
				//				Toast.makeText(getActivity(), "" 
				//                        + position, Toast.LENGTH_SHORT).show();
			}
		});
	}

}


